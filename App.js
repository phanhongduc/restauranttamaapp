/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

//import from libraries
import React, { Component } from "react";
import {
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator,
  createBottomTabNavigator,
  NavigationActions,
  StackActions
} from "react-navigation";
import { Provider } from "mobx-react";
//import from source
//------Auth Stack-------
import SignInScreen from "./src/screens/Auth/SignIn";
import ForgotScreen from "./src/screens/Auth/ForgotPassword";
import TermConditionScreen from "./src/screens/Auth/TermsCondition";
import Etape1Screen from "./src/screens/Auth/Etape1";
import CustomizePinCode from "./src/screens/Auth/CustomizePinCode";
import ConfirmedScreen from "./src/screens/Auth/Confirmed";
//-------Main Stack-------
import RestaurantDetailScreen from "./src/screens/Main/HomeTab/RestaurantDetail";
import QRScanScreen from "./src/screens/Main/QRScanTab/QRScan";
import PaymentAmountScreen from "./src/screens/Main/QRScanTab/PaymentAmount";
import PaymentPincodeScreen from "./src/screens/Main/QRScanTab/PaymentPincode";
import ProgressScreen from "./src/screens/Main/QRScanTab/Progress";
import PaymentSuccessScreen from "./src/screens/Main/QRScanTab/PaymentSuccess";
import PaymentFailureScreen from "./src/screens/Main/QRScanTab/PaymentFailure";
import SettingScreen from "./src/screens/Main/ProfileTab/Setting";
import ChangePincodeScreen from "./src/screens/Main/ProfileTab/ChangePincode";
import MyQRCodeScreen from "./src/screens/Main/ProfileTab/MyQRCode";
import HistoryScreen from "./src/screens/Main/ProfileTab/History";
import MesInfosScreen from "./src/screens/Main/ProfileTab/MesInfos";
import ContactUsScreen from "./src/screens/Main/ProfileTab/ContactUs";
import HelpScreen from "./src/screens/Main/ProfileTab/Help";
import AboutUsScreen from "./src/screens/Main/ProfileTab/AboutUs";
//import components
import Navbar from "./src/components/Navbar";
//import constants
import { fonts, colors, fontSizes } from "./src/assets/constants";
//import stores
import keyboardStore from "./src/stores/KeyboardStore";
import customizePinCodeStore from "./src/stores/CustomizePinCodeStore";
import changePincodeStore from "./src/stores/ChangePincodeStore";
import paymentAmountStore from "./src/stores/PaymentAmountStore";
import paymentPincodeStore from "./src/stores/PaymentPincodeStore";
import etape1Store from "./src/stores/Etape1Store";
import ProfileStore from "./src/stores/ProfileStore";
import termConditionStore from "./src/stores/TermsConditionStore";
import MesInfosStore from "./src/stores/MesInfosStore";
import restaurantDetailStore from "./src/stores/RestaurantDetailStore";
import headerTitleStore from "./src/stores/HeaderTitleStore";
import restaurantCardItemStore from "./src/stores/RestaurantCardItemStore";
import navbarStore from "./src/stores/NavbarStore";
import historyScreenStore from "./src/stores/HistoryStore";
import restaurantStore from "./src/stores/RestaurantStore";

const stores = {
  keyboardStore,
  customizePinCodeStore,
  changePincodeStore,
  paymentAmountStore,
  paymentPincodeStore,
  etape1Store,
  ProfileStore,
  termConditionStore,
  MesInfosStore,
  restaurantDetailStore,
  headerTitleStore,
  restaurantCardItemStore,
  navbarStore,
  historyScreenStore,
  restaurantStore
};

const AuthStack = createStackNavigator(
  {
    SignIn: {
      screen: SignInScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Forgot: {
      screen: ForgotScreen
    },
    Terms: {
      screen: TermConditionScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Etape1: {
      screen: Etape1Screen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    CustomizePinCode: {
      screen: CustomizePinCode,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Confirmed: {
      screen: ConfirmedScreen
    }
  },
  {
    initialRouteName: "SignIn",
    defaultNavigationOptions: {
      headerBackTitle: null,
      headerStyle: {
        backgroundColor: colors.primary.orange,
        shadowOffset: {
          width: 0,
          height: 0
        },
        elevation: 0
      },
      headerTintColor: colors.text.white.normal,
      headerTitleStyle: {
        fontFamily: fonts.primary.semibold,
        fontSize: fontSizes.header,
        color: colors.text.white.normal
      }
    },
    headerLayoutPreset: "center"
  }
);

const defaultGetStateForActionAuthStack = AuthStack.router.getStateForAction;
AuthStack.router.getStateForAction = (action, state) => {
  if (!state || action.type !== NavigationActions.BACK) {
    return defaultGetStateForActionAuthStack(action, state);
  }

  const { routeName } = state.routes[state.index];
  switch (routeName) {
    case "Confirmed":
      return {
        ...state,
        routes: [
          {
            routeName: "SignIn"
          }
        ],
        index: 0
      };
    case "Terms":
      return defaultGetStateForActionAuthStack(action, state);
    case "Etape1":
      etape1Store.handleHardwareBack();
      return null;
    case "CustomizePinCode":
      if (
        customizePinCodeStore.numberFailed >=
        customizePinCodeStore.MAXIMUM_FAILURE
      ) {
        return;
      } else if (
        customizePinCodeStore.inputType ===
        customizePinCodeStore.INPUT_CONFIRM_PINCODE
      ) {
        customizePinCodeStore.handleReInputPincode();
        return null;
      }
      return defaultGetStateForActionAuthStack(action, state);
    default:
      return defaultGetStateForActionAuthStack(action, state);
  }
};

const QRStack = createStackNavigator(
  {
    QRScan: {
      screen: QRScanScreen
    },
    PaymentAmount: {
      screen: PaymentAmountScreen
    },
    PaymentPincode: {
      screen: PaymentPincodeScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Progress: {
      screen: ProgressScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    PaymentSuccess: {
      screen: PaymentSuccessScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    PaymentFailure: {
      screen: PaymentFailureScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    }
  },
  {
    initialRouteName: "QRScan",
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerBackTitle: null,
        headerStyle: {
          backgroundColor: colors.primary.orange,
          shadowOffset: {
            width: 0,
            height: 0
          },
          elevation: 0
        },
        headerTintColor: colors.text.white.normal,
        headerTitleStyle: {
          fontFamily: fonts.primary.semibold,
          fontSize: fontSizes.header,
          color: colors.text.white.normal
        }
      };
    },
    headerLayoutPreset: "center"
  }
);
QRStack.navigationOptions = ({ navigation }) => {
  const tabBarVisible =
    navigation.state.routes[navigation.state.index].routeName === "QRScan";

  return {
    tabBarVisible
  };
};

const defaultGetStateForActionQRStack = QRStack.router.getStateForAction;
QRStack.router.getStateForAction = (action, state) => {
  if (!state || action.type !== NavigationActions.BACK) {
    return defaultGetStateForActionQRStack(action, state);
  }
  const { routeName } = state.routes[state.index];
  switch (routeName) {
    case "QRScan":
      navbarStore.setSelectedTab("HomeTab");
      return defaultGetStateForActionQRStack(action, state);
    case "PaymentPincode":
      paymentPincodeStore.toggleCancelModal();
      return null;
    case "Progress":
      return null;
    case "PaymentSuccess":
    case "PaymentFailure":
      const { navigation } = state.routes[state.index].params;
      navigation.popToTop();
      navbarStore.setSelectedTab("HomeTab");
      const resetAction = StackActions.reset({
        key: "HomeTab",
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "RestaurantDetail" })]
      });
      navigation.dispatch(resetAction);
      return null;
    default:
      return defaultGetStateForActionQRStack(action, state);
  }
};

const ProfileStack = createStackNavigator(
  {
    Profile: {
      screen: SettingScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    MyQRCode: {
      screen: MyQRCodeScreen
    },
    History: {
      screen: HistoryScreen
    },
    MesInfos: {
      screen: MesInfosScreen
    },
    ChangePincode: {
      screen: ChangePincodeScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    ContactUs: {
      screen: ContactUsScreen
    },
    Help: {
      screen: HelpScreen
    },
    AboutUs: {
      screen: AboutUsScreen
    }
  },
  {
    initialRouteName: "Profile",
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerBackTitle: null,
        headerStyle: {
          backgroundColor: colors.primary.orange,
          shadowOffset: {
            width: 0,
            height: 0
          },
          elevation: 0
        },
        headerTintColor: colors.text.white.normal,
        headerTitleStyle: {
          fontFamily: fonts.primary.semibold,
          fontSize: fontSizes.header,
          color: colors.text.white.normal
        }
      };
    },
    headerLayoutPreset: "center"
  }
);
ProfileStack.navigationOptions = ({ navigation }) => {
  const { routeName } = navigation.state.routes[navigation.state.index];
  const tabBarVisible = routeName !== "ChangePincode";

  return {
    tabBarVisible
  };
};

const RestaurantsStack = createStackNavigator(
  {
    RestaurantDetail: {
      screen: RestaurantDetailScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => {
      return {
        headerBackTitle: null,
        headerStyle: {
          backgroundColor: colors.primary.orange,
          shadowOffset: {
            width: 0,
            height: 0
          },
          elevation: 0
        },
        headerTintColor: colors.text.white.normal,
        headerTitleStyle: {
          fontFamily: fonts.primary.semibold,
          fontSize: fontSizes.header,
          color: colors.text.white.normal
        }
      };
    },
    headerLayoutPreset: "center"
  }
);

const MainStack = createBottomTabNavigator(
  {
    HomeTab: {
      screen: RestaurantsStack
    },
    QRScanTab: {
      screen: QRStack
    },
    ProfileTab: {
      screen: ProfileStack
    }
  },
  {
    initialRouteName: "HomeTab",
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: colors.primary.orange,
        shadowOffset: {
          width: 0,
          height: 0
        },
        elevation: 0
      },
      headerTintColor: colors.text.white.normal,
      headerTitleStyle: {
        fontFamily: fonts.primary.semibold,
        fontSize: fontSizes.header,
        color: colors.text.white.normal
      }
    },
    headerMode: "screen",
    headerLayoutPreset: "center",
    tabBarComponent: Navbar
  }
);

const AppStack = createSwitchNavigator(
  {
    Auth: {
      screen: AuthStack
    },
    Main: {
      screen: MainStack
    }
  },
  {
    initialRouteName: "Auth"
    // initialRouteName: "Main"
  }
);

const AppContainer = createAppContainer(AppStack);

export default class App extends Component {
  render() {
    return (
      <Provider {...stores}>
        <AppContainer />
      </Provider>
    );
  }
}
