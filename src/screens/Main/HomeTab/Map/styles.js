import { StyleSheet, Dimensions, Platform } from "react-native";
 const {width, height} = Dimensions.get('window');
 const ANNOTATION_SIZE = 45;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  matchParent: {
    flex: 1,
  },
  modalBoxWrap: {
    position: "absolute",
    borderRadius: 6,
    top: (height * 50) / 100,
    width: width,
    height: (height * 50) / 100,
    flex: 1,
    backgroundColor: "transparent",
    zIndex: 10,
  },
  wrap: {
    flex: 1,
    zIndex: 9999,
    position: "relative",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255,255,255, 1)",
    borderRadius: 6,
    paddingTop: 0,
    paddingRight: 0,
    paddingBottom: 0,
    paddingLeft: 0,
  },
  layoutBox: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 20,
    paddingRight: 20,
    paddingBottom: 20,
    paddingLeft: 20,
    marginTop: 10,
  },
  hiddenModal:{
    height:50
  },
  RestaurantCardItemModal:{
    height:'70%'
  },
  map:{
    ...StyleSheet.absoluteFillObject,
        width: '100%',
        height: '100%',
        top: 40,
        zIndex: 1
  },
  annotationContainer: {
    width: ANNOTATION_SIZE,
    height: ANNOTATION_SIZE,
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: 'white',
    //borderRadius: ANNOTATION_SIZE / 2,
    //borderWidth: StyleSheet.hairlineWidth,
    //borderColor: 'rgba(0, 0, 0, 0.45)',
    opacity: 1
  },
  annotationFill: {
    width: ANNOTATION_SIZE - 3,
    height: ANNOTATION_SIZE - 3,
    borderRadius: (ANNOTATION_SIZE - 3) / 2,
    backgroundColor: 'orange',
    transform: [{scale: 0.6}],
  },
  imageMarker: {
    flex: 1,
    resizeMode: 'contain',
    width: 45,
    height: 45,
    transform: [{scale: 0.8}],
    opacity: 1
  },
  containerCallout:{
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: 120,
    zIndex: 9999999,
    marginVertical: 5, 
    ...Platform.select({
      ios:{
        position: 'absolute', 
        top: 42, 
        left: -60, 
        right:0
      },
    })
  },
  titleCallout: {
    fontSize: 16, 
    fontWeight: 'bold', 
    fontFamily: "Montserrat-SemiBold", 
    textAlign: 'center',
    color: '#111'
  }
});

export default styles;
