import React, { Component, PureComponent } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  Image,
  Platform,
  PermissionsAndroid,
  Keyboard,
  Alert,
  Animated,
  TouchableHighlight,
  Easing
} from "react-native";
import { observer, inject } from "mobx-react";
import styles from "./styles";
import MapboxGL from "@mapbox/react-native-mapbox-gl";
import Modal from "react-native-modalbox";
import RestaurantCardItem from "./../../../../components/RestaurantCardItem/default";
import SearchBar from "./../../../../components/SearchBar";
import axios from "axios";
import geoViewport from "@mapbox/geo-viewport";
import { SERVER_URL } from "./../../../../ultils/API";

const { width, height } = Dimensions.get("window");
const CENTER_COORD = [-149.7915448, -17.5772494];
const MAPBOX_VECTOR_TILE_SIZE = 512;

const removeDiacritics = (input) => {
  var output = "";
  var normalized = input.normalize("NFD");
  var i = 0;
  var j = 0;
  while (i < input.length) {
    output += normalized[j];
    j += (input[i] == normalized[j]) ? 1 : 2;
    i++;
  }
  return output;
}

export default class Map extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: "offlinePack",
      offlineRegion: null,
      offlineRegionStatus: null,
      restaurants: [],
      showMap: true,
      modalVisible: false,
      searchText: "",
      activeAnnotationIndex: -1,
      previousActiveAnnotationIndex: -1,
      restaurant: null,
      fistLoading: true,
    };

    this._scaleIn = null;
    this._scaleOut = null;
    this._opacityIn = null;
    this._opacityOut = null;
    this.animatedValue = new Animated.Value(0)
    this.imageOpacityValue = new Animated.Value(1)

    MapboxGL.setAccessToken(
      "pk.eyJ1IjoidHJ1b25ndm5tIiwiYSI6ImNqd3N6YWw4ZjA5c2QzenFrdWE3Y2NteHgifQ.Rw1iHaaUSh1b6pa8w6Di7A"
    );
    this.onDownloadProgress = this.onDownloadProgress.bind(this);
    this.onDidFinishLoadingStyle = this.onDidFinishLoadingStyle.bind(this);
    this.onStatusRequest = this.onStatusRequest.bind(this);
    this.hiddenKeyBoardHandler = this.hiddenKeyBoardHandler.bind(this);
    this.moveToMarker = this.moveToMarker.bind(this);
    this.moveToMarkerProps = this.moveToMarkerProps.bind(this);
  }

  async componentDidMount() {
    await this.getDataRestaurant();
    this.getLocationHandler();
  }
  componentWillUnmount() {
    Keyboard.dismiss();
  }

  async onDidFinishLoadingStyle() {
    const { width, height } = Dimensions.get('window');
    const bounds = geoViewport.bounds(
      CENTER_COORD,
      12,
      [width, height],
      MAPBOX_VECTOR_TILE_SIZE,
    );
    const options = {
      name: this.state.name,
      styleURL: MapboxGL.StyleURL.Street,
      bounds: [[bounds[0], bounds[1]], [bounds[2], bounds[3]]],
      minZoom: 10,
      maxZoom: 20,
    };
    MapboxGL.offlineManager.setTileCountLimit(1000);
    const offlinePack = await MapboxGL.offlineManager.getPack("offlinePack");
    if (!offlinePack) {
      // start download
      MapboxGL.offlineManager.createPack(
        options,
        this.onDownloadProgress,
      );
    } else {
      if (this.state.offlineRegion) {
        this.state.offlineRegion.resume();
      }
      await offlinePack.resume();
    }
  }
  async getDataRestaurant() {
    const apiURL = SERVER_URL + "/api/restaurants";
    const response = await axios.get(apiURL, {
      headers: {
        Accept: "application/json"
      }
    })
      .then(res => {
        const restaurants = res.data;
        this.setState({ restaurants });
      })
      .catch(err => {
        Alert.alert("Cannot load data from server.");
      });

  }
  onDownloadProgress(offlineRegion, offlineRegionStatus) {
    this.setState({
      name: offlineRegion.name,
      offlineRegion: offlineRegion,
      offlineRegionStatus: offlineRegionStatus
    });
  }

  getLocationHandler = async () => {
    this.setState({ showMap: true });
    if (Platform.OS === "android") {
      const permission = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      );

      if (permission === PermissionsAndroid.RESULTS.GRANTED) {
        this.getCurrentLocation();
      } else {
        this.moveToMarker(
          this.state.restaurants[0].longitude,
          this.state.restaurants[0].latitude
        );
      }
    } else {
      this.getCurrentLocation();
    }
  };

  getCurrentLocation = async () => {
    try {
      navigator.geolocation.getCurrentPosition(
        pos => {
          const coordinate = {
            latitude: pos.coords.latitude,
            longitude: pos.coords.longitude
          };
          //this.moveToMarker(coordinate.longitude, coordinate.latitude);
          this.moveToMarker(
            this.state.restaurants[0].longitude,
            this.state.restaurants[0].latitude
          );
        },
        err => {
          console.log(err);
          alert("Fetching the Position failed, please pick one manually!");
        }
      );
    } catch (err) {
      console.log("No Access  to location" + err);
    }
  };

  async onStatusRequest() {
    if (this.state.offlineRegion) {
      const offlineRegionStatus = await this.state.offlineRegion.status();
      Alert.alert("Get Status", JSON.stringify(offlineRegionStatus, null, 2));
    }
  }
  onAnnotationDeselected(deselectedIndex) {
    const nextState = {};
    if (this.state.activeAnnotationIndex === deselectedIndex) {
      nextState.activeAnnotationIndex = -1;
    }
    this.opacityAllMarker();
    this._scaleOut = new Animated.Value(1);
    Animated.timing(this._scaleOut, { toValue: 0.8, duration: 200 }).start();
    this._opacityOut = new Animated.Value(1);
    Animated.timing(this._opacityOut, { toValue: 0.5, duration: 200 }).start();
    nextState.previousActiveAnnotationIndex = deselectedIndex;
    this.setState(nextState);
  }
  onAnnotationSelected(activeIndex, item) {
    if (this.state.activeIndex === activeIndex) {
      return;
    }

    this._scaleIn = new Animated.Value(0.8);
    Animated.timing(this._scaleIn, { toValue: 1.0, duration: 200 }).start();
    this._opacityIn = new Animated.Value(0);
    Animated.timing(this._opacityIn, { toValue: 1, duration: 200 }).start();

    const restaurant = {
      id: item.id,
      thumbnail: item.images && item.images[0],
      name: item.restaurantName,
      images: item.images,
      facebook: item.facebook,
      instagram: item.instagram,
      website: item.website,
      email: item.email,
      phoneNumber: [item.phoneNumber1, item.phoneNumber2],
      openingHours: item.openingHours
    };
    this.resetAllMarker();

    this.setState({ activeAnnotationIndex: activeIndex, restaurant });

    if (this.state.previousActiveAnnotationIndex !== -1) {
      this._map.moveTo([item.longitude, item.latitude], 500);
    }
    Keyboard.dismiss();
    this.openModal();
  }
  renderAnnotation(counter) {
    this.setState({ fistLoading: false })
    const id = `pointAnnotation${counter}`;
    const coordinate = [
      this.state.restaurants[counter].longitude,
      this.state.restaurants[counter].latitude
    ];
    const title = this.state.restaurants[counter].restaurantName;
    const animationStyle = {};
    const opacityStyle = {};
    let uri = require('./../../../../assets/ic_marker_open.png');
    /*if (this.state.restaurants[counter]) {
      uri = require('./../../../../assets/ic_marker_open.png');
    } else {
      uri = require('./../../../../assets/ic_marker_close.png');
    }*/
    const itemId = `pointAnnotation${this.state.restaurants[counter].id}`
    this.animatedValue[itemId] = new Animated.Value(0);

    const animateItemMarker = this.animatedValue[itemId].interpolate({
      inputRange: [0, 1],
      outputRange: [0.8, 1]
    });
    const imageOpacity = this.imageOpacityValue[itemId].interpolate({
      inputRange: [0, 1],
      outputRange: [0.5, 1]
    });
    if (animateItemMarker) {
      animationStyle.transform = [{ scale: animateItemMarker }];
    }
    if (imageOpacity) {
      opacityStyle.opacity = imageOpacity;
    }

    if (counter === this.state.activeAnnotationIndex) {
      animationStyle.transform = [{ scale: this._scaleIn }];
      opacityStyle.opacity = this._opacityIn;
    } else if (counter === this.state.previousActiveAnnotationIndex) {
      animationStyle.transform = [{ scale: this._scaleOut }];
      // opacityStyle.opacity = this._opacityOut;
    }
    return (

      <MapboxGL.PointAnnotation
        key={id}
        id={id}
        title={title}
        coordinate={coordinate}
        onSelected={() => this.onAnnotationSelected(counter, this.state.restaurants[counter])}
        onDeselected={() => this.onAnnotationDeselected(counter)}
      //style={{ position: 'relative', left: 0, right: 0 }}

      >
        <Animated.View>
          <TouchableOpacity style={[styles.annotationContainer, {}]} onPress={() => this.onAnnotationSelected(counter, this.state.restaurants[counter])} >
            <Animated.Image
              source={uri}
              style={[styles.imageMarker, { transform: [{ scale: animateItemMarker }] }, animationStyle, opacityStyle]} />
          </TouchableOpacity>
        </Animated.View>
        <MapboxGL.Callout tipStyle={{ borderTopColor: 'transparent' }} contentStyle={{ backgroundColor: 'transparent', borderWidth: 0 }} textStyle={{ fontSize: 16, fontWeight: 'bold', fontFamily: "Montserrat-SemiBold" }} >
          <View style={styles.containerCallout}>
            <Text style={styles.titleCallout}>{title}</Text>
          </View>
        </MapboxGL.Callout>
      </MapboxGL.PointAnnotation>
    );
  }

  renderAnnotations() {
    const items = [];
    for (let i = 0; i < this.state.restaurants.length; i++) {
      if (this.state.fistLoading) {
        const itemId = `pointAnnotation${this.state.restaurants[i].id}`;
        this.imageOpacityValue[itemId] = new Animated.Value(1);
      }
      items.push(this.renderAnnotation(i));
    }
    return items;
  }
  closeModal = () => {
    this.modal.close();
  };

  openModal() {
    this.modal.open();
  }
  onSearchTextChange = text => {
    this.setState({ searchText: text });
  };
  findRestaurant(searchText) {
    if (searchText === "") {
      return [];
    }
    const { restaurants } = this.state;
    const regex = new RegExp(`${searchText.trim()}`, "i");
    return restaurants.filter(item =>
      removeDiacritics(item.restaurantName).search(regex) >= 0 || removeDiacritics(item.address).search(regex) >= 0
    );
  }
  moveToMarker = (lng, lat) => {
    this._map.setCamera({
      centerCoordinate: [lng, lat],
      zoom: 15,
      duration: 500
    });
  }

  opacityAllMarker() {
    const { restaurants } = this.state;
    restaurants.map(el => {
      let itemId = `pointAnnotation${el.id}`
      this.imageOpacityValue[itemId] = new Animated.Value(1);
    })
  }
  resetAllMarker() {
    const { restaurants } = this.state;
    restaurants.map(el => {
      let itemId = `pointAnnotation${el.id}`
      this.imageOpacityValue[itemId] = new Animated.Value(0);
      Animated.timing(this.animatedValue[itemId], {
        toValue: 0,
        duration: 500,
        easing: Easing.ease
      }).start();
    })
  }
  moveToMarkerProps = (lng, lat, id) => {
    this._map.setCamera({
      centerCoordinate: [lng, lat],
      zoom: 15,
      duration: 200,
    })

    const itemId = `pointAnnotation${id}`
    Animated.sequence([
      Animated.timing(this.animatedValue[itemId], {
        toValue: 1,
        duration: 500,
        easing: Easing.ease
      }),
      Animated.timing(this.imageOpacityValue[itemId], {
        toValue: 1,
        duration: 600,
        easing: Easing.ease
      })
    ]).start();

  }
  hiddenKeyBoardHandler = () => {
    Keyboard.dismiss();
  };
  navigateToDetail = restaurant => {
    const { navigation } = this.props;
    navigation.navigate("RestaurantDetail", { restaurant: restaurant });
  };
  render() {
    const {
      restaurants,
      showMap,
      offlineRegionStatus,
      searchText,
      restaurant
    } = this.state;
    const restaurantSearchItems = this.findRestaurant(searchText);

    return (
      <View style={{ flex: 1 }}>
        <View
          style={{
            width: "100%",
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            zIndex: 2
          }}
        >
          <SearchBar
            value={searchText}
            onChangeText={this.onSearchTextChange}
            items={restaurantSearchItems}
            moveToMarker={this.moveToMarkerProps}
          />
        </View>
        <View style={{ flex: 1, width: "100%" }}>
          {showMap && (
            <MapboxGL.MapView
              zoomLevel={15}
              ref={c => (this._map = c)}
              onPress={this.onPress}
              onDidFinishLoadingMap={this.onDidFinishLoadingStyle}
              centerCoordinate={CENTER_COORD}
              // showUserLocation={true}
              userTrackingMode={1}
              attributionEnabled={false}
              logoEnabled={false}
              rotateEnabled={false}
              animated={true}
              style={styles.map}
              onPress={this.hiddenKeyBoardHandler}
            >
              {this.renderAnnotations()}
            </MapboxGL.MapView>
          )}
        </View>
        <Modal
          ref={modal => (this.modal = modal)}
          animationDuration={100}
          backdropOpacity={0.5}
          position="top"
          isOpen={false}
          style={styles.modalBoxWrap}
        >
          <View style={[styles.wrap, { backgroundColor: "#FFF" }]}>
            <TouchableOpacity onPress={this.closeModal}>
              <View style={styles.hiddenModal}>
                <Image
                  source={require("./../../../../assets/ic_chevron_up.png")}
                  style={{
                    flex: 1,
                    resizeMode: "contain",
                    width: 20,
                    height: 20
                  }}
                />
              </View>
            </TouchableOpacity>
            <View style={styles.RestaurantCardItemModal}>
              <TouchableOpacity
                style={styles.cardWrapper}
                onPress={this.navigateToDetail.bind(this, restaurant)}
              >
                <RestaurantCardItem restaurant={restaurant} />
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
