import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { LargeList } from "react-native-largelist-v3";
import { NormalHeader } from "react-native-spring-scrollview/NormalHeader";
import { inject, observer } from "mobx-react";
import RestaurantCardItem from "./../../../../components/RestaurantCardItem";
import styles from "./styles";

@inject("restaurantStore")
@observer
export default class Restaurants extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.restaurantStore.fetchListRestaurants();
  }

  navigateToDetail = restaurant => {
    const { navigation } = this.props;
    navigation.navigate("RestaurantDetail", { restaurant: restaurant });
    navigation.navigate("test", { restaurant: restaurant });
    // restaurants test detail
  };

  renderItem = ({ section, row }) => {
    const item = this.props.restaurantStore.data[section].items[row];
    const restaurant = {
      id: item.id,
      thumbnail: item.images && item.images[0],
      name: item.restaurantName,
      images: item.images,
      facebook: item.facebook,
      instagram: item.instagram,
      website: item.website,
      email: item.email,
      phoneNumber: [item.phoneNumber1, item.phoneNumber2],
      openingHours: item.openingHours
    };
    return (
      <TouchableOpacity
        style={styles.cardWrapper}
        onPress={this.navigateToDetail.bind(this, restaurant)}
      >
        <RestaurantCardItem restaurant={restaurant} />
      </TouchableOpacity>
    );
  };

  onRefresh = () => {
    this.props.restaurantStore.fetchListRestaurants(() => {
      this.listRef.endRefresh();
    });
  };

  renderListRestaurants = () => {
    const { restaurantStore } = this.props;
    const result = !restaurantStore.isLoading ? (
      <LargeList
        ref={ref => (this.listRef = ref)}
        data={restaurantStore.data}
        heightForSection={() => 50}
        renderSection={this.renderSection}
        heightForIndexPath={() => 76}
        renderIndexPath={this.renderItem}
        refreshHeader={NormalHeader}
        onRefresh={this.onRefresh}
      />
    ) : null;
    return result;
  };

  renderSection = section => {
    const { restaurantStore } = this.props;
    return (
      <View style={styles.sectionContainer}>
        <Text style={styles.sectionText}>
          {restaurantStore.data[section].section}
        </Text>
      </View>
    );
  };

  render() {
    return <View style={styles.container}>{this.renderListRestaurants()}</View>;
  }
}
