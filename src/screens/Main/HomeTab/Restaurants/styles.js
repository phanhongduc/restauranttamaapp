import { StyleSheet } from "react-native";
import { colors, fontSizes, fonts } from "./../../../../assets/constants";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: colors.background.gray.bar,
    paddingTop: 20,
    paddingBottom: 100
  },
  cardWrapper: {
    alignItems: "center"
  },
  sectionContainer: {
    minHeight: 8,
    marginHorizontal: "9%",
    backgroundColor: colors.background.gray.bar
  },
  sectionText: {
    marginBottom: 10,
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.header,
    color: colors.text.gray.title
  }
});

export default styles;
