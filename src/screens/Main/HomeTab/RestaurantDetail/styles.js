import { StyleSheet, Dimensions } from "react-native";
import { Header } from "react-navigation";
import {
  fontSizes,
  fonts,
  colors,
  getReponsive
} from "./../../../../assets/constants";

const { height, width } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;
const headerHeight = mainScreenHeight * 0.1986;

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: colors.background.white
  },
  headerWrapper: {
    height: getReponsive.height(141),
    alignItems: "center"
  },
  cardContainer: {
    height: getReponsive.height(67),
    marginTop: getReponsive.height(43)
  },
  cardInfoContainer: {
    height: headerHeight * 0.5 * 0.8
  },
  nameStyle: {
    fontSize: fontSizes.header,
    fontFamily: fonts.primary.bold
  },
  socialAction: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: colors.background.gray.bar,
    height: getReponsive.height(94)
  },
  icon: {
    width: getReponsive.width(17),
    height: getReponsive.height(22)
  },
  iconSocialWrapper: {
    height: "50%",
    aspectRatio: 1 / 1,
    justifyContent: "center",
    alignItems: "center"
  },
  infoDetailContainer: {
    marginTop: getReponsive.height(24),
    marginBottom: getReponsive.height(33),
    justifyContent: "center"
  },
  infoDetailWrapper: {
    justifyContent: "space-evenly",
    alignItems: "center",
    marginBottom: getReponsive.height(47)
  },
  infoDetailDescription: {
    marginTop: getReponsive.height(24),
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.title,
    textAlign: "center"
  },
  infoDetailOpenTime: {
    marginTop: getReponsive.height(5),
    fontFamily: fonts.primary.semibold,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.title,
    textAlign: "center"
  },
  infoDetailPhoneNumberWrapper: {
    flexDirection: "row"
  },
  infoDetailPhoneNumber: {
    marginTop: getReponsive.height(5),
    marginBottom: getReponsive.height(5),
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.title
    // color: colors.text.orange
  },
  infoDetailAddress: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.orange,
    textAlign: "center"
  },
  columnStyle: {
    justifyContent: "flex-start"
    // justifyContent: "space-between"
  },
  separator: {
    height: width * 0.0155
  },
  buttonPickPhoto: {
    width: getReponsive.width(122),
    height: getReponsive.height(128),
    marginRight: getReponsive.borderWidth(5),
    marginBottom: getReponsive.borderWidth(5),
    backgroundColor: colors.text.gray.icon,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonAddImageStyle: {
    width: getReponsive.width(73),
    height: getReponsive.width(72)
  },
  imageStyle: {
    width: getReponsive.width(122),
    height: getReponsive.height(128),
    marginRight: getReponsive.borderWidth(5),
    marginBottom: getReponsive.borderWidth(5)
  },
  blankArea: {
    height: 100
  },
  modal: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.background.overlayDark
  },
  modalGalleryContainer: {
    justifyContent: "center",
    alignItems: "center",
    height: height * 0.9,
    width: width
  },
  modalWrapper: {
    marginTop: height * 0.1,
    height: height * 0.7
  },
  modalSlider: {
    overflow: "visible",
    marginTop: 20
  },
  modalImage: {
    height: width
  },
  modalLeftCloseButton: {
    position: "absolute",
    left: 20,
    top: 0,
    height: 40
  },
  modalRightCloseButton: {
    position: "absolute",
    right: 20,
    top: 0,
    height: 40
  }
});

export default styles;
