import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Modal,
  Dimensions,
  Platform,
  Linking,
  Alert
} from "react-native";
import FastImage from "react-native-fast-image";
import Carousel from "react-native-snap-carousel";
import IconM from "react-native-vector-icons/MaterialIcons";
import IconA from "react-native-vector-icons/AntDesign";
import IconZ from "react-native-vector-icons/Zocial";
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import { observer, inject } from "mobx-react";
import RestaurantCardItem from "./../../../../components/RestaurantCardItem";
import { colors } from "./../../../../assets/constants";
import styles from "./styles";

const { width } = Dimensions.get("window");
const isIOS = Platform.OS === "ios";

@inject("restaurantDetailStore", "headerTitleStore", "restaurantStore")
@observer
export default class RestaurantDetailScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Restaurant"
    };
  };

  constructor(props) {
    super(props);

    // sudo restaurant info
    this.state = {
      restaurant: null
    };
  }

  // sudo get restaurant
  async getRestaurant() {
    const { restaurantStore } = this.props;
    await restaurantStore.fetchListRestaurants();
    return restaurantStore.listRestaurants[0];
  }

  async componentDidMount() {
    const { headerTitleStore } = this.props;
    headerTitleStore.setTitle("Restaurant");

    // sudo set Restaurant
    let restaurant = await this.getRestaurant();
    this.setState({ restaurant: restaurant });
  }

  renderButtonPickPhoto() {
    return (
      <TouchableOpacity
        style={styles.buttonPickPhoto}
        onPress={this.handleOnPickPhoto.bind(this)}
      >
        <FastImage
          style={styles.buttonAddImageStyle}
          source={require("../../../../assets/ic_add_photo.png")}
          resizeMode={FastImage.resizeMode.stretch}
        />
      </TouchableOpacity>
    );
  }

  renderItem = ({ item, index }) => {
    const { restaurantDetailStore } = this.props;
    if (!item) return this.renderButtonPickPhoto();
    const marginRight = !((index + 1) % 3) ? { marginRight: 0 } : {};
    return (
      <TouchableOpacity
        onPress={() => restaurantDetailStore.onPressImage(index - 1)}
      >
        <FastImage
          style={{
            ...styles.imageStyle,
            ...marginRight
          }}
          source={{ uri: item }}
          resizeMode={FastImage.resizeMode.stretch}
        />
      </TouchableOpacity>
    );
  };

  // renderSeparator = () => <View style={styles.separator} />;

  renderGalleryModal = () => {
    const { restaurantDetailStore, navigation } = this.props;

    // sudo get restaurant
    const restaurant = {
      ...this.state.restaurant
    };
    if (!this.state.restaurant) return null;

    return (
      <Modal
        visible={restaurantDetailStore.openGalleryModal}
        animationType="fade"
        transparent
        hardwareAccelerated
      >
        <View style={styles.modal}>
          <View style={styles.modalGalleryContainer}>
            <View style={styles.modalWrapper}>
              <Carousel
                sliderWidth={width}
                sliderHeight={width * 2}
                itemWidth={width * 0.9}
                data={restaurant.images.slice()}
                renderItem={this.renderItemGalleryModal}
                inactiveSlideScale={0.94}
                inactiveSlideOpacity={0}
                containerCustomStyle={styles.modalSlider}
                firstItem={restaurantDetailStore.selectedImgIdx}
              />
            </View>
            <TouchableOpacity
              style={
                isIOS
                  ? styles.modalLeftCloseButton
                  : styles.modalRightCloseButton
              }
              onPress={restaurantDetailStore.toggleGalleryModal}
            >
              <IconM
                name="close"
                size={30}
                color={colors.background.gray.dot}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  renderItemGalleryModal = ({ item }) => {
    return (
      <View style={styles.item}>
        <Image
          source={{ uri: item }}
          style={styles.modalImage}
          resizeMode="contain"
        />
      </View>
    );
  };

  handleOnPressMail = mail => {
    if (!mail) return;
    Linking.canOpenURL(`mailto:${mail}`)
      .then(supported => {
        if (supported) {
          Linking.openURL(`mailto:${mail}`);
        } else {
          Linking.openURL(
            "https://accounts.google.com/ServiceLogin?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1"
          );
        }
      })
      .catch(error => console.log(error));
  };

  handleOnPressFacebook = facebookURL => {
    if (!facebookURL) return;
    Linking.canOpenURL(facebookURL)
      .then(supported => {
        if (supported) {
          Linking.openURL(facebookURL);
        }
        // else {
        //   Linking.openURL(`https://www.facebook.com/${facebookURL}`);
        // }
      })
      .catch(error => console.log(error));
  };

  handleOnPressInstagram = instagramUsername => {
    if (!instagramUsername) return;
    Linking.canOpenURL(`instagram://user?username=${instagramUsername}`)
      .then(supported => {
        if (supported) {
          Linking.openURL(`instagram://user?username=${instagramUsername}`);
        } else {
          Linking.openURL(`https://www.instagram.com/${instagramUsername}`);
        }
      })
      .catch(error => console.log(error));
  };

  handleOnOpenWebsite = website => {
    if (!website) return;
    Linking.canOpenURL(website)
      .then(supported => {
        if (supported) {
          Linking.openURL(website);
        }
      })
      .catch(error => console.log(error));
  };

  handleOnMakeCall = phoneNumber => {
    const actionURL = `tel:${phoneNumber}`;
    Linking.canOpenURL(actionURL)
      .then(supported => {
        if (!supported) {
          Alert.alert("Phone number is not available");
        } else {
          return Linking.openURL(actionURL);
        }
      })
      .catch(err => console.log(err));
  };

  handleOnPickPhoto = async () => {
    // sudo get restaurant
    let restaurant = {
      ...this.state.restaurant
    };
    if (!this.state.restaurant) return;

    const { restaurantStore } = this.props;
    await restaurantStore.pickPhotos(restaurant.id);
  };

  render() {
    // sudo get restaurant
    let restaurant = {
      ...this.state.restaurant
    };
    if (!this.state.restaurant) return null;

    const photos = [...[""], ...restaurant.images.slice()];
    restaurant = {
      ...restaurant,
      name: restaurant.restaurantName,
      thumbnail: !!restaurant.images ? restaurant.images[0] : "",
      phoneNumber: [restaurant.phoneNumber1, restaurant.phoneNumber2]
    };

    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={styles.headerWrapper}>
          <RestaurantCardItem
            containerStyle={styles.cardContainer}
            infoContainerStyle={styles.cardInfoContainer}
            nameStyle={styles.nameStyle}
            restaurant={restaurant}
          />
        </View>
        <View style={styles.socialAction}>
          <TouchableOpacity style={styles.iconSocialWrapper}>
            <Image
              style={styles.icon}
              source={require("../../../../assets/ic_profile_location.png")}
              resizeMode="stretch"
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.iconSocialWrapper}
            onPress={this.handleOnPressMail.bind(this, restaurant.email)}
          >
            <IconZ name="email" size={27} color={colors.text.gray.tab} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.iconSocialWrapper}
            onPress={this.handleOnPressFacebook.bind(this, restaurant.facebook)}
          >
            <IconFA5 name="facebook" size={25} color={colors.text.gray.tab} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.iconSocialWrapper}
            onPress={this.handleOnPressInstagram.bind(
              this,
              restaurant.instagram
            )}
          >
            <IconFA5 name="instagram" size={25} color={colors.text.gray.tab} />
          </TouchableOpacity>
        </View>
        <View style={styles.infoDetailContainer}>
          <View style={styles.infoDetailWrapper}>
            <Text style={styles.infoDetailDescription}>
              Promenade de Nice Papeete
            </Text>
            <Text style={styles.infoDetailOpenTime}>
              Lundi à vendredi: 05:30 - 18:00
            </Text>
            <Text style={styles.infoDetailOpenTime}>Samedi: 05:30 - 14:30</Text>
            <Text style={styles.infoDetailPhoneNumber}>
              {restaurant.phoneNumber[0]}
            </Text>
            <TouchableOpacity
              onPress={this.handleOnPressMail.bind(this, restaurant.email)}
            >
              <Text style={styles.infoDetailAddress}>{restaurant.email}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.handleOnPressFacebook.bind(
                this,
                restaurant.website
              )}
            >
              <Text style={styles.infoDetailAddress}>{restaurant.website}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <FlatList
          style={{ width: width }}
          columnWrapperStyle={styles.columnStyle}
          keyExtractor={(item, index) => index.toString()}
          // data={restaurant.images.slice()}
          data={photos}
          renderItem={this.renderItem}
          numColumns={3}
          // ItemSeparatorComponent={this.renderSeparator}
          scrollEnabled={false}
        />
        <View style={styles.blankArea} />
        {this.renderGalleryModal()}
      </ScrollView>
    );
  }
}
