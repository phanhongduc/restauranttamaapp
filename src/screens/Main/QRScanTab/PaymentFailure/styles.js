import { StyleSheet, Dimensions } from "react-native";
import {
  fontSizes,
  fonts,
  colors,
  getReponsive
} from "../../../../assets/constants";
import { Header } from "react-navigation";

const { width, height } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  wrapper: {
    width: "100%",
    height: mainScreenHeight * 0.6211,
    justifyContent: "space-between",
    alignItems: "center"
  },
  title: {
    width: width * 0.5,
    textAlign: "center",
    color: colors.text.black,
    fontSize: fontSizes.detail.largeTitle,
    fontFamily: fonts.primary.medium
  },
  failureIcon: {
    // width: width * 0.4586,
    // height: width * 0.4586
    width: 172,
    height: 172
  },
  button: {
    width: getReponsive.width(175),
    height: getReponsive.height(52)
  }
});

export default styles;
