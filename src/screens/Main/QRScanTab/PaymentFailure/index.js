import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import Sound from "react-native-sound";
import { NavigationActions, StackActions } from "react-navigation";
import { inject, observer } from "mobx-react";
import Button from "./../../../../components/Button";
import styles from "./styles";

const failureIcon = require("./../../../../assets/ic_transaction_fail.png");

@inject("navbarStore")
@observer
export default class PaymentFailureScreen extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    headerTitle: "Échec de la transaction",
    headerLeft: null
  };

  componentDidMount() {
    this.props.navigation.setParams({
      navigation: this.props.navigation
    });

    // Enable playback in silence mode
    Sound.setCategory("Playback");
    this.errorSound = new Sound(
      "sound_failure.mp3",
      Sound.MAIN_BUNDLE,
      error => {
        if (error) {
          console.log("Failed to load the sound", error);
          return;
        }
        this.errorSound.play(success => {
          if (success) {
            // console.log(success);
          } else {
            console.log("something went wrong");
          }
        });
      }
    );
  }

  componentWillUnmount() {
    // Release the audio player resource
    // This should be released when user close app
    this.errorSound.release();
  }

  handleBackToMainScreen = () => {
    const { navigation, navbarStore } = this.props;
    navigation.popToTop();
    navbarStore.setSelectedTab("HomeTab");
    const resetAction = StackActions.reset({
      key: "HomeTab",
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "RestaurantDetail" })]
    });
    navigation.dispatch(resetAction);
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Text style={styles.title}>La transaction n’a pas abouti</Text>
          <Image source={failureIcon} style={styles.failureIcon} />
          <Button
            style={styles.button}
            title="OK"
            onPress={this.handleBackToMainScreen}
          />
        </View>
      </View>
    );
  }
}
