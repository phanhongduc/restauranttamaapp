import React, { Component } from "react";
import { View, Text, TouchableOpacity, Modal } from "react-native";
import { observer, inject } from "mobx-react";
import { NavigationActions, StackActions } from "react-navigation";
import Icon from "react-native-vector-icons/Feather";
import paymentPincodeStore from "./../../../../stores/PaymentPincodeStore";
import Keyboard from "./../../../../components/Keyboard";
import Button from "./../../../../components/Button";
import styles from "./styles";
import { colors } from "../../../../assets/constants";

@inject("keyboardStore", "paymentPincodeStore", "navbarStore")
@observer
export default class PaymentPincodeScreen extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    headerTitle: "Patachoux Tahiti",
    headerLeft: (
      <TouchableOpacity
        style={styles.cancelButtonWrapper}
        onPress={paymentPincodeStore.toggleCancelModal}
      >
        <Text style={styles.cancelButtonTitle}>Annuler</Text>
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: colors.primary.green,
      shadowOffset: { width: 0, height: 0 },
      elevation: 0
    }
  };

  componentDidMount() {
    this.props.paymentPincodeStore.initScreen();
  }

  handleOnDelete = () => {
    const { keyboardStore, paymentPincodeStore } = this.props;
    keyboardStore.onDelete(() => {
      paymentPincodeStore.removeDigit();
    });
  };

  handleOnValidate = () => {
    this.props.navigation.navigate("Progress");
  };

  handleCancelPayment = () => {
    const { paymentPincodeStore } = this.props;

    paymentPincodeStore.toggleCancelModal();
    this.handleBackToMainScreen();
  };

  handleBackToMainScreen = () => {
    const { navigation, navbarStore } = this.props;
    navigation.popToTop();
    navbarStore.setSelectedTab("Hometab");
    const resetAction = StackActions.reset({
      key: "HomeTab",
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "RestaurantDetail" })]
    });
    navigation.dispatch(resetAction);
  };

  renderBalanceModal = () => {
    const { paymentPincodeStore } = this.props;

    return (
      <Modal
        visible={paymentPincodeStore.openBalanceModal}
        animationType="fade"
        transparent
        hardwareAccelerated
      >
        <View style={styles.modalContainer}>
          <View style={styles.balanceModalContainer}>
            <View style={styles.balanceModalContentWrapper}>
              <Text style={styles.balanceModalTitle}>
                {`Solde disponible à\ndépenser aujourd’hui\n à 12:45`}
              </Text>
              <Text style={styles.balanceModalAmount}>X XXX FCP</Text>
            </View>
            <View style={styles.modalButtonWrapper}>
              <TouchableOpacity
                style={styles.modalButton}
                onPress={paymentPincodeStore.toggleBalanceModal}
              >
                <Text style={styles.modalButtonTitle}>Fermer</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  renderCancleModal = () => {
    const { paymentPincodeStore } = this.props;

    return (
      <Modal
        visible={paymentPincodeStore.openCancelModal}
        animationType="fade"
        transparent
        hardwareAccelerated
      >
        <View style={styles.modalContainer}>
          <View style={styles.closeModalContainer}>
            <View style={styles.closeModalContentWrapper}>
              <Text style={styles.closeModalTitle}>
                Confirmez-vous l’annulation ?
              </Text>
            </View>
            <View style={styles.modalButtonWrapper}>
              <TouchableOpacity
                style={[styles.modalButton, styles.rightVerticalRule]}
                onPress={paymentPincodeStore.toggleCancelModal}
              >
                <Text style={styles.modalButtonTitle}>Non</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.modalButton}
                onPress={this.handleCancelPayment}
              >
                <Text style={styles.modalButtonTitle}>Oui</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    const { paymentPincodeStore, keyboardStore } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.amountContainer}>
          <Text style={styles.amountTitle}>MONTANT À PAYER</Text>
          <Text style={styles.amountText}>700 FCP</Text>
        </View>
        <Text style={styles.title}>SAISISSEZ VOTRE CODE TAMA'A</Text>
        <View style={styles.input}>
          {keyboardStore.length ? (
            <View style={styles.pincodeTextContainer}>
              <View style={styles.pincodeTextWrapper}>
                <Text style={styles.pincodeText}>
                  {paymentPincodeStore.pincodeViewStr}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.deleteIconWrapper}
                onPress={this.handleOnDelete}
              >
                <Icon name="delete" size={25} style={styles.deleteIcon} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.underline} />
          )}
        </View>
        <Keyboard
          color={colors.text.green}
          onPress={paymentPincodeStore.addDigit}
        />
        <TouchableOpacity
          style={styles.viewBalanceContainer}
          onPress={paymentPincodeStore.toggleBalanceModal}
        >
          <Text style={styles.viewBalanceText}>Voir mon solde</Text>
        </TouchableOpacity>
        <Button
          title="Valider"
          disabled={paymentPincodeStore.disableButton}
          style={[
            styles.button,
            { backgroundColor: colors.primary.green },
            paymentPincodeStore.disableButton && styles.disabledButton
          ]}
          onPress={this.handleOnValidate}
        />
        {this.renderBalanceModal()}
        {this.renderCancleModal()}
      </View>
    );
  }
}
