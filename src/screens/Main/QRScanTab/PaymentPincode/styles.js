import { StyleSheet, Dimensions } from "react-native";
import {
  fontSizes,
  fonts,
  colors,
  getReponsive
} from "../../../../assets/constants";
import { Header } from "react-navigation";

const { height, width } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-evenly",
    backgroundColor: colors.secondary
  },
  amountContainer: {
    height: mainScreenHeight * 0.08,
    justifyContent: "center",
    alignItems: "center"
  },
  amountTitle: {
    color: colors.text.black,
    fontSize: fontSizes.detail.big,
    fontFamily: fonts.primary.semibold
  },
  amountText: {
    color: colors.text.black,
    fontSize: 25,
    fontFamily: fonts.primary.medium
  },
  input: {
    height: "7%",
    width: "40%",
    maxHeight: 60,
    alignItems: "center"
    // backgroundColor: '#f00'
  },
  titleWrapper: {
    alignItems: "center"
  },
  title: {
    color: colors.text.black,
    fontSize: fontSizes.title,
    fontFamily: fonts.primary.bold,
    marginTop: mainScreenHeight * 0.05
  },
  pincodeTextContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  pincodeTextWrapper: {
    alignItems: "center",
    width: "88%",
    height: "100%",
    paddingRight: "2%"
    // backgroundColor: '#ff0'
  },
  pincodeText: {
    fontSize: 30,
    color: colors.text.black,
    fontFamily: fonts.primary.bold
    // backgroundColor: '#f00',
  },
  underline: {
    height: getReponsive.height(55),
    width: getReponsive.width(219),
    borderBottomWidth: getReponsive.borderWidth(1),
    borderColor: colors.text.black
  },
  balance: {
    fontSize: fontSizes.header,
    fontFamily: fonts.primary.semibold,
    color: colors.text.black
  },
  deleteIconWrapper: {
    height: "100%",
    paddingTop: 5,
    paddingLeft: 10
  },
  deleteIcon: {
    color: colors.text.gray.icon
  },
  disabledButton: {
    backgroundColor: colors.disabled
  },
  viewBalanceContainer: {
    // backgroundColor: "#ff0",
    height: mainScreenHeight * 0.03,
    justifyContent: "center"
  },
  viewBalanceText: {
    color: colors.text.gray.title,
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.regular,
    textDecorationLine: "underline"
  },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    paddingTop: Header.HEIGHT + mainScreenHeight * 0.2,
    backgroundColor: colors.background.overlay
  },
  balanceModalContainer: {
    width: width * 0.72, // TODO: set maxWidth for modalContainer
    maxWidth: 300,
    aspectRatio: 3 / 2,
    borderRadius: 12,
    backgroundColor: colors.background.white
  },
  balanceModalContentWrapper: {
    flex: 3,
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  balanceModalTitle: {
    color: colors.text.black,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.secondary.medium,
    textAlign: "center"
    // width: "57.7%"
  },
  balanceModalAmount: {
    color: colors.text.black,
    fontSize: fontSizes.detail.lightBig,
    fontFamily: fonts.secondary.medium
  },
  modalButtonWrapper: {
    flex: 1,
    flexDirection: "row",
    borderTopWidth: 0.5,
    borderTopColor: colors.text.gray.border
  },
  modalButton: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  rightVerticalRule: {
    borderRightWidth: 0.5,
    borderRightColor: colors.text.gray.border
  },
  modalButtonTitle: {
    color: colors.text.blue,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.secondary.medium
  },
  closeModalContainer: {
    width: width * 0.72,
    maxWidth: 300,
    aspectRatio: 135 / 52,
    borderRadius: 12,
    backgroundColor: colors.background.white
  },
  closeModalContentWrapper: {
    height: "57%",
    justifyContent: "center",
    alignItems: "center"
  },
  closeModalTitle: {
    color: colors.text.black,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.secondary.medium,
    textAlign: "center"
    // width: "78.5%"
  },
  cancelButtonWrapper: {
    marginLeft: 20,
    height: 20
  },
  cancelButtonTitle: {
    color: colors.text.darkGreen,
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.bold
  },
  button: {
    width: getReponsive.width(175),
    height: getReponsive.height(52)
  }
});

export default styles;
