import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions
} from "react-native";
import { inject, observer } from "mobx-react";
import InstructionScreen from "./../Instruction";
import QRCodeScanner from "react-native-qrcode-scanner";
import ScannerMarker from "../../../../components/ScannerMarker";
import styles from './styles';

const { width, height } = Dimensions.get("window");

@observer
export default class QRScanScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      shouldRender:true
    }
  }

  static navigationOptions = {
    headerTitle: "Scannez le QR code"
  };

  componentDidMount () {
    this.removeWillFocusListener = this.props.navigation.addListener(
      'willFocus', () => {this.setState({shouldRender:true})}
    )

    this.removeWillBlurListener = this.props.navigation.addListener(
      'willBlur', () => {this.setState({shouldRender:false})}
    )
  }

  componentWillUnmount () {
    // this.removeWillFocusListener()
    // this.removeWillBlurListener()
  }

  onSuccess = e => {
    this.props.navigation.navigate("PaymentAmount", {
      data: e.data,
      scanner: this.scanner
    });
  }

  render() {
    return (
      <View style={{ 
          flex: 1, 
          alignItems: "center", 
        }}>
        {this.state.shouldRender && <QRCodeScanner
          onRead={this.onSuccess}
          ref={node => {
            this.scanner = node;
          }}
          cameraStyle={{ 
            height: height,
          }}
          markerStyle = {styles.marker}
          reactivate={true}
          showMarker={true}
          checkAndroid6Permissions={true}
          customMarker = {<ScannerMarker/>}
        />
        }
        <InstructionScreen 
          pagekey='isFirstTime'
        />
      </View>
    );
  }
}
