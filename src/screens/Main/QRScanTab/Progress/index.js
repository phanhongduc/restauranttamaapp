import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { inject, observer } from "mobx-react";
import styles from "./styles";

const loadingIcon = require("../../../../assets/Loader_transaction_restaurateur.gif");

@inject("navbarStore")
@observer
export default class ProgressScreen extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    headerTitle: "Autorisation en cours",
    headerLeft: null
  };

  componentDidMount() {
    setTimeout(() => {
      // this.props.navigation.navigate("PaymentFailure");
      this.props.navigation.navigate("PaymentSuccess");
    }, 2000);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <Image
            source={loadingIcon}
            style={styles.icon}
            resizeMode="stretch"
          />
          <Text style={styles.text}>
            Transaction en cours Veuillez patienter
          </Text>
        </View>
      </View>
    );
  }
}
