import { StyleSheet, Dimensions } from "react-native";
import { Header } from "react-navigation";
import { fontSizes, fonts, colors } from "./../../../../assets/constants";

const { height, width } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#fbfbfb"
  },
  contentContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: mainScreenHeight * 0.1
  },
  icon: {
    width: width * 0.792,
    height: width * 0.792
  },
  text: {
    marginTop: height * 0.03,
    textAlign: "center",
    width: width * 0.5,
    color: colors.text.gray.title,
    fontSize: fontSizes.detail.huge,
    fontFamily: fonts.primary.medium
  }
});

export default styles;
