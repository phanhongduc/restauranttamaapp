import { StyleSheet, Dimensions, Platform } from "react-native";
import {
  fonts,
  colors,
  fontSizes,
  getReponsive
} from "./../../../../assets/constants";

const screen = Dimensions.get("window");
const styles = StyleSheet.create({
  wrapper: {
    height: getReponsive.height(812)
  },
  instructionView: { justifyContent: "center", alignItems: "center" },
  instructionTitle: {
    fontFamily: fonts.primary.bold,
    color: colors.text.black,
    fontSize: fontSizes.bigtitle,
    textAlign: "center",
    marginTop: getReponsive.height(124)
  },
  instructionImage: {},
  image: {
    marginTop: getReponsive.height(46),
    width: getReponsive.width(324),
    height: getReponsive.height(247)
  },
  instructionText: {
    marginTop: getReponsive.height(31),
    fontSize: fontSizes.detail.huge,
    textAlign: "center",
    fontFamily: fonts.primary.semibold,
    color: colors.text.black
  },
  activeDotStyle: {
    width: getReponsive.height(12),
    height: getReponsive.height(12),
    borderRadius: getReponsive.borderRadius(6),
    // marginHorizontal: 8,
    backgroundColor: colors.background.orange
  },
  inactiveDotStyle: {
    backgroundColor: colors.background.gray.dot,
    width: getReponsive.width(12),
    height: getReponsive.width(12),
    borderRadius: getReponsive.borderRadius(6)
    // marginHorizontal: 8
  },
  paginationStyle: { backgroundColor: colors.background.white },
  instructionModalView: {
    // width: screen.width,
    // height: screen.height,
    backgroundColor: colors.background.white,
    justifyContent: "center",
    alignItems: "center"
  },
  paginationView: { paddingTop: 10, paddingBottom: 10 },
  proceedButtonView: {
    // width: "90%",
    width: getReponsive.width(214),
    // height: 50,
    height: getReponsive.height(52),
    backgroundColor: colors.background.orange,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: getReponsive.borderRadius(26),
    marginBottom: getReponsive.height(63)
    // borderRadius: 30
    // marginTop: 10,
    // marginBottom: 30
  },
  proceedButtonText: {
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.detail.huge,
    color: colors.text.white.normal
  },
  emptyButtonView: {
    width: "90%",
    height: getReponsive.height(115)
    // marginTop: getReponsive.height(10),
    // marginBottom: getReponsive.height(30)
  },
  closeButtonView: {
    position: "absolute",
    top: screen.height > 600 ? 40 : 20,
    right: screen.height > 600 ? 40 : 20
  },
  closeButtonStyle: { flex: 1 }
});

export default styles;
