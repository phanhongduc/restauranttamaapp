import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Modal,
  Dimensions
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { observer } from "mobx-react";
import { fonts, colors, fontSizes } from "./../../../../assets/constants";
import styles from "./styles";

const screen = Dimensions.get("window");

const instructions = [
  {
    name: "Etape 1",
    data: "Scannez le QR code \ndu client"
  },
  {
    name: "Etape 2",
    data: "Saisissez le montant \nde la commande"
  },
  {
    name: "Etape 3",
    data: "Le client entre son \ncode Tama'a pour \nvalider la transaction"
  },
  {
    name: "Transaction acceptée",
    data: "Le justificatif de \npaiement s'affiche \nà l'écran"
  }
];

@observer
export default class Instruction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      activeSlide: 0
    };
  }

  handleClose() {
    this.setModalVisible(false);
  }

  setModalVisible(state) {
    this.setState({ modalVisible: state });
  }

  componentDidMount() {
    AsyncStorage.getItem(this.props.pagekey, (error, result) => {
      if (error) {
      } else {
        if (result === null) {
          this.setModalVisible(true);
        }
      }
    });
    AsyncStorage.setItem(this.props.pagekey, JSON.stringify({ value: true }));
  }

  renderItem({ item, index }) {
    return (
      <View key={"instruction_slide_" + index} style={styles.instructionView}>
        <Text style={styles.instructionTitle}>{item.name}</Text>
        <View style={styles.instructionImage}>
          {index === 0 && (
            <Image
              style={styles.image}
              source={require("../../../../assets/ig_instruction_1.png")}
              resizeMode="stretch"
            />
          )}
          {index === 1 && (
            <Image
              style={styles.image}
              source={require("../../../../assets/ig_instruction_2.png")}
              resizeMode="stretch"
            />
          )}
          {index === 2 && (
            <Image
              style={styles.image}
              source={require("../../../../assets/ig_instruction_3.png")}
              resizeMode="stretch"
            />
          )}
          {index === 3 && (
            <Image
              style={styles.image}
              source={require("../../../../assets/ig_instruction_4.png")}
              resizeMode="stretch"
            />
          )}
        </View>
        <View>
          <Text style={styles.instructionText}>{item.data}</Text>
        </View>
      </View>
    );
  }

  get pagination() {
    return (
      <Pagination
        dotsLength={instructions.length}
        activeDotIndex={this.state.activeSlide}
        containerStyle={styles.paginationStyle}
        dotStyle={styles.activeDotStyle}
        inactiveDotStyle={styles.inactiveDotStyle}
        inactiveDotOpacity={0.7}
        inactiveDotScale={1}
      />
    );
  }

  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => this.setModalVisible(false)}
        >
          <View style={styles.wrapper}>
            <View style={styles.instructionModalView}>
              <Carousel
                ref={c => (this.carousel = c)}
                data={instructions}
                renderItem={this.renderItem}
                activeDotIndex={index => index}
                onSnapToItem={index => this.setState({ activeSlide: index })}
                // sliderWidth={screen.width * 0.8}
                // itemWidth={screen.width * 0.8}
                sliderWidth={screen.width}
                itemWidth={screen.width}
                removeClippedSubviews={false}
              />
              <View style={styles.paginationView}>{this.pagination}</View>
              {this.state.activeSlide === instructions.length - 1 ? (
                <TouchableOpacity
                  style={styles.proceedButtonView}
                  onPress={() => this.setModalVisible(false)}
                >
                  <Text style={styles.proceedButtonText}>
                    Lancez un paiement
                  </Text>
                </TouchableOpacity>
              ) : (
                <View style={styles.emptyButtonView} />
              )}
            </View>
            <TouchableOpacity
              style={styles.closeButtonView}
              onPress={() => {
                this.handleClose();
              }}
            >
              <Image
                source={require("./../../../../assets/ic_close.png")}
                style={styles.closeButtonStyle}
                resizeMode={"contain"}
              />
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}
