import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { observer, inject } from "mobx-react";
import Icon from "react-native-vector-icons/Feather";
import HeaderLeft from "./../../../../components/HeaderLeft";
import Keyboard from "./../../../../components/Keyboard";
import Button from "./../../../../components/Button";
import { colors } from "./../../../../assets/constants";
import styles from "./styles";

@inject("keyboardStore", "paymentAmountStore")
@observer
export default class PaymentAmountScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scanner: undefined
    };
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: navigation.state.params.data,
      headerStyle: {
        backgroundColor: colors.primary.orange,
        shadowOffset: { width: 0, height: 0 },
        elevation: 0
      },
      headerLeft: (
        <HeaderLeft
          navigation={navigation}
          goBack={() => navigation.goBack()}
        />
      )
    };
  };

  componentDidMount() {
    const { navigation, paymentAmountStore } = this.props;
    paymentAmountStore.initScreen(navigation);
  }

  handleOnDelete = () => {
    const { keyboardStore, paymentAmountStore } = this.props;
    keyboardStore.onDelete(() => {
      paymentAmountStore.removeDigit();
    });
  };

  handleOnValidate = () => {
    this.props.paymentAmountStore.handleOnValidate();
  };

  render() {
    const { paymentAmountStore, keyboardStore } = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.title}>MONTANT À PAYER</Text>
        <View style={styles.input}>
          {keyboardStore.length ? (
            <View style={styles.amountTextContainer}>
              <View style={styles.amountTextWrapper}>
                <Text style={styles.amountText}>
                  {paymentAmountStore.amountViewStr}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.deleteIconWrapper}
                onPress={this.handleOnDelete}
              >
                <Icon name="delete" size={25} style={styles.deleteIcon} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.underline} />
          )}
        </View>
        <Text style={styles.balance}>
          Solde disponible : {paymentAmountStore.balance} FCP
        </Text>
        <Keyboard
          onPress={paymentAmountStore.addDigit}
          color={styles.keyboardOrange.color}
        />
        <Button
          title="Valider"
          disabled={paymentAmountStore.disableButton}
          style={[
            styles.button,
            paymentAmountStore.disableButton
              ? styles.disabledButton
              : styles.buttonOrange
          ]}
          onPress={this.handleOnValidate}
        />
      </View>
    );
  }
}
