import { StyleSheet, Dimensions } from "react-native";
import {
  fontSizes,
  fonts,
  colors,
  getReponsive
} from "../../../../assets/constants";
import { Header } from "react-navigation";

const { height } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: colors.secondary
  },
  input: {
    height: "8%",
    width: "65%",
    maxHeight: 60,
    alignItems: "center"
    // backgroundColor: '#f00'
  },
  titleWrapper: {
    alignItems: "center"
  },
  title: {
    color: colors.text.black,
    fontSize: fontSizes.title,
    fontFamily: fonts.primary.semibold,
    marginTop: mainScreenHeight * 0.05
  },
  amountTextContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  amountTextWrapper: {
    alignItems: "center",
    width: "88%",
    height: "100%",
    paddingRight: "2%"
    // backgroundColor: '#ff0'
  },
  amountText: {
    fontSize: 45,
    color: colors.text.black,
    fontFamily: fonts.primary.medium
    // backgroundColor: '#f00',
  },
  underline: {
    height: getReponsive.height(55),
    width: getReponsive.width(219),
    borderBottomWidth: getReponsive.borderWidth(1),
    borderColor: colors.text.black
  },
  balance: {
    fontSize: fontSizes.header,
    fontFamily: fonts.primary.semibold,
    color: colors.text.black
  },
  deleteIconWrapper: {},
  deleteIcon: {
    color: colors.text.gray.icon
  },
  disabledButton: {
    backgroundColor: colors.disabled
  },
  keyboardOrange: {
    color: colors.primary.orange
  },
  buttonOrange: {
    backgroundColor: colors.primary.orange
  },
  button: {
    width: getReponsive.width(175),
    height: getReponsive.height(52)
  }
});

export default styles;
