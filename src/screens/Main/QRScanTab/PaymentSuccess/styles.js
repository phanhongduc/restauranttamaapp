import { StyleSheet, Dimensions } from "react-native";
import { Header } from "react-navigation";
import {
  fontSizes,
  fonts,
  colors,
  getReponsive
} from "./../../../../assets/constants";

const { height, width } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  wrapper: {
    height: mainScreenHeight * 0.76,
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center"
    // backgroundColor: '#ff0'
  },
  successIcon: {
    // width: width * 0.4586,
    // height: width * 0.4586
    width: 172,
    height: 172
  },
  instructionText: {
    width: width * 0.55,
    textAlign: "center",
    color: colors.text.black,
    fontSize: fontSizes.detail.largeTitle,
    fontFamily: fonts.primary.medium
    // backgroundColor: '#f00'
  },
  contentContainer: {
    height: mainScreenHeight * 0.18,
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  successTitle: {
    textAlign: "center",
    color: colors.text.green,
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.regular
  },
  amountText: {
    textAlign: "center",
    color: colors.text.black,
    fontSize: fontSizes.detail.extremeLarge,
    fontFamily: fonts.primary.regular
  },
  timeText: {
    width: width * 0.5,
    textAlign: "center",
    color: colors.text.black,
    fontSize: fontSizes.detail.normal,
    fontFamily: fonts.primary.regular
  },
  button: {
    width: getReponsive.width(175),
    height: getReponsive.height(52)
  }
});

export default styles;
