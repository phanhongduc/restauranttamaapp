import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import Sound from "react-native-sound";
import { NavigationActions, StackActions } from "react-navigation";
import { inject, observer } from "mobx-react";
import Button from "./../../../../components/Button";
import styles from "./styles";
import { colors } from "../../../../assets/constants";

const successIcon = require("./../../../../assets/ig_transaction_ok.png");

@inject("navbarStore")
@observer
export default class PaymentSuccessScreen extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    headerTitle: "Transaction acceptée",
    headerLeft: null,
    headerStyle: {
      backgroundColor: colors.primary.green,
      shadowOffset: { width: 0, height: 0 },
      elevation: 0
    }
  };

  componentDidMount() {
    this.props.navigation.setParams({
      navigation: this.props.navigation
    });

    // Enable playback in silence mode
    Sound.setCategory("Playback");
    this.successSound = new Sound(
      "sound_success.mp3",
      Sound.MAIN_BUNDLE,
      error => {
        if (error) {
          console.log("Failed to load the sound", error);
          return;
        }
        this.successSound.play(success => {
          if (success) {
            // console.log(success);
          } else {
            console.log("something went wrong");
          }
        });
      }
    );
  }

  componentWillUnmount() {
    // Release the audio player resource
    // This should be released when user close app
    this.successSound.release();
  }

  handleBackToMainScreen = () => {
    const { navigation, navbarStore } = this.props;
    navigation.popToTop();
    navbarStore.setSelectedTab("HomeTab");
    const resetAction = StackActions.reset({
      key: "HomeTab",
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "RestaurantDetail" })]
    });
    navigation.dispatch(resetAction);
  };

  handleOnPressValidate = () => {
    this.handleBackToMainScreen();
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Text style={styles.instructionText}>
            Présentez cet écran au restaurateur
          </Text>
          <Image source={successIcon} stle={styles.successIcon} />
          <View style={styles.contentContainer}>
            <Text style={styles.successTitle}>VOUS VENEZ DE VERSER</Text>
            <Text style={styles.amountText}>700 FCP</Text>
            <Text style={styles.timeText}>
              à Patachoux Tahiti le 09/04/19 à 12:45
            </Text>
          </View>
          <Button
            title="Valider"
            style={[styles.button, { backgroundColor: colors.primary.green }]}
            onPress={this.handleOnPressValidate}
          />
        </View>
      </View>
    );
  }
}
