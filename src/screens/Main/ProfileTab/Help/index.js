import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { inject, observer } from "mobx-react";
import HeaderLeft from "./../../../../components/HeaderLeft";
import styles from "./styles";

const icon = require("./../../../../assets/ic_logo_contact.png");

@observer
export default class HelpScreen extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Aide",
      headerLeft: <HeaderLeft goBack={() => navigation.goBack()} />
    };
  };

  render() {
    return (
      <View style={styles.container}>
        <Image source={icon} />
        <View style={styles.infoWrapper}>
          <Text style={styles.info}>patachouxtahiti@gmail.com</Text>
          <Text style={styles.info}>87 70 21 42</Text>
        </View>
      </View>
    );
  }
}
