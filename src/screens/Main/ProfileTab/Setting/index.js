import React, { Component } from "react";
import { View, TouchableOpacity, Text, Modal, ScrollView } from "react-native";
import Button from "./../../../../components/Button";
import IconMC from "react-native-vector-icons/MaterialCommunityIcons";
import IconF from "react-native-vector-icons/Feather";
import IconO from "react-native-vector-icons/Octicons";
import IconI from "react-native-vector-icons/Ionicons";
import { inject, observer } from "mobx-react";
import HeaderLeft from "./../../../../components/HeaderLeft";
import SettingCardItem from "./../../../../components/SettingCardItem";
import styles from "./styles";
import { colors } from "./../../../../assets/constants";

@observer
export default class SettingScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Profil"
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      logoutSelected: false
    };
  }

  setModalVisible = state => {
    this.setState({ logoutSelected: state });
  };

  confirmExit = () => {
    try {
      this.setModalVisible(false);
      this.props.navigation.navigate("SignIn");
    } catch (exception) {
      console.log(`error`, JSON.stringify(exception));
    }
  };

  renderHistoryIcon = () => (
    <IconMC name="restore-clock" size={24} color={colors.text.gray.menuIcon} />
  );

  renderMyInfoIcon = () => (
    <IconF name="user" size={24} color={colors.text.gray.menuIcon} />
  );

  renderPincodeIcon = () => (
    <IconMC name="grid" size={20} color={colors.text.gray.menuIcon} />
  );

  renderContactIcon = () => (
    <IconO name="mail" size={22} color={colors.text.gray.menuIcon} />
  );

  renderHelpIcon = () => (
    <IconI name="ios-search" size={23} color={colors.text.gray.menuIcon} />
  );

  renderAboutUsIcon = () => (
    <IconO name="info" size={24} color={colors.text.gray.menuIcon} />
  );

  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <View style={styles.wrapper}>
            <SettingCardItem
              title="Historique"
              renderIcon={this.renderHistoryIcon}
              page="History"
              navigation={this.props.navigation}
            />
            <SettingCardItem
              title="Mes infos"
              renderIcon={this.renderMyInfoIcon}
              page="MesInfos"
              navigation={this.props.navigation}
            />
            <SettingCardItem
              title="Changer le code Tama'a"
              renderIcon={this.renderPincodeIcon}
              page="ChangePincode"
              navigation={this.props.navigation}
            />
            <SettingCardItem
              title="Nous contacter"
              renderIcon={this.renderContactIcon}
              page="ContactUs"
              navigation={this.props.navigation}
            />
            <SettingCardItem
              title="Aide"
              renderIcon={this.renderHelpIcon}
              page="Help"
              navigation={this.props.navigation}
            />
            <SettingCardItem
              title="À propos"
              renderIcon={this.renderAboutUsIcon}
              page="AboutUs"
              navigation={this.props.navigation}
            />
            <TouchableOpacity
              style={styles.cardItemView}
              onPress={() => {
                this.setModalVisible(true);
              }}
            >
              <View style={styles.cardItemTitleView}>
                <View style={styles.cardIconWrapper}>
                  <IconF
                    name="log-out"
                    size={22}
                    color={colors.text.gray.menuIcon}
                  />
                </View>
                <Text style={styles.cardItemText}>Se déconnecter</Text>
              </View>
              <View style={styles.cardItemIconView}>
                <IconMC
                  name="chevron-right"
                  size={35}
                  color={colors.text.gray.chevronRight}
                />
              </View>
            </TouchableOpacity>
            <Button
              labelStyle={styles.btnText}
              style={styles.buttonContainer}
              onPress={() => this.props.navigation.navigate("MyQRCode")}
              title={"QR Code"}
            />
            <Modal
              visible={this.state.logoutSelected}
              transparent={true}
              hardwareAccelerated={true}
              animationType="fade"
            >
              <View style={styles.modalContainer}>
                <View style={styles.modalWrapper}>
                  <View style={styles.modalContentContainer}>
                    <Text style={styles.modalTitle}>
                      Voulez-vous vraiment vous déconnecter?
                    </Text>
                  </View>
                  <View style={styles.modalButtonContainer}>
                    <TouchableOpacity
                      style={styles.modalCancelButton}
                      onPress={() => this.setModalVisible(false)}
                    >
                      <Text style={styles.modalButtonLabel}>Non</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.modalConfirmButton}
                      onPress={this.confirmExit}
                    >
                      <Text style={styles.modalButtonLabel}>Oui</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
          </View>
        </View>
      </ScrollView>
    );
  }
}
