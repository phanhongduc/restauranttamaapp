import { StyleSheet, Dimensions } from "react-native";
import {
  colors,
  fontSizes,
  fonts,
  getReponsive
} from "./../../../../assets/constants";
import { Header } from "react-navigation";

const screen = Dimensions.get("window");
const mainScreenHeight = screen.height - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // flexGrow: 1,
    backgroundColor: colors.background.nearwhite
    // paddingTop: 10
    // paddingTop: 40
  },
  wrapper: {
    alignItems: "center",
    marginTop: getReponsive.height(76),
    marginBottom: getReponsive.height(140)
  },
  cardItemView: {
    // width: screen.width * 0.8,
    width: getReponsive.width(300),
    height: getReponsive.height(52),
    // height: 50,
    backgroundColor: colors.background.white,
    borderRadius: getReponsive.borderRadius(6),
    padding: getReponsive.borderWidth(10),
    // borderRadius: 10,
    // margin: 10,
    // padding: 10,
    // paddingLeft: 20,
    // paddingRight: 10,
    marginBottom: getReponsive.height(9),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  cardItemTitleView: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    flex: 3
  },
  cardIconWrapper: {
    flex: 1
  },
  cardItemText: {
    flex: 4,
    fontFamily: fonts.primary.medium,
    color: colors.text.gray.icon,
    fontSize: fontSizes.detail.big
  },
  cardItemIconView: {
    flex: 1,
    alignItems: "flex-end"
  },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.background.overlay
  },
  modalWrapper: {
    width: screen.width * 0.72,
    maxWidth: 300,
    aspectRatio: 135 / 52,
    borderRadius: 12,
    backgroundColor: colors.background.white
  },
  modalContentContainer: {
    flex: 1,
    alignItems: "center",
    padding: 10
  },
  modalTitle: {
    width: "100%",
    color: colors.text.black,
    fontSize: fontSizes.detail.big2,
    fontFamily: fonts.secondary.regular,
    textAlign: "center"
  },
  modalButtonContainer: {
    flex: 1,
    flexDirection: "row",
    // justifyContent: "space-between",
    // alignItems: "center",
    borderTopWidth: getReponsive.borderWidth(1),
    borderColor: colors.text.gray.border
  },
  modalConfirmButton: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  modalCancelButton: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderRightWidth: getReponsive.borderWidth(1),
    borderColor: colors.text.gray.border
  },
  modalButtonLabel: {
    color: colors.text.blue,
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.secondary.medium
  },
  btnText: {
    fontSize: fontSizes.detail.huge
  },
  buttonContainer: {
    marginTop: getReponsive.height(25),
    height: getReponsive.height(52),
    // height: mainScreenHeight * 0.38 - 100,
    width: getReponsive.width(175),
    // width: screen.width,
    alignItems: "center"
    // borderTopWidth: 1,
    // borderTopColor: colors.text.gray.line,
  }
});

export default styles;
