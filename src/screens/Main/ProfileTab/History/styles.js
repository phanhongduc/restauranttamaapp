import { StyleSheet } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import {
  fonts,
  colors,
  fontSizes,
  getReponsive
} from "../../../../assets/constants";

const styles = StyleSheet.create({
  wrapper: {
    height: 64,
    // alignItems: "center",
    // justifyContent: "center",
    width: "100%"
    // flex: 1
  },
  swiper: {
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    // height: 100
    flex: 1
  },
  child: {
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    flex: 1
  },
  slide: {
    justifyContent: "center",
    alignItems: "center",
    // flex: 1
    width: "100%",
    height: "100%"
    // paddingTop: 8
  },
  text: {
    color: colors.text.black,
    fontSize: fontSizes.detail.big2,
    fontFamily: fonts.primary.regular
  },
  wrapButtonText: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 12,
    height: "100%"
  },
  clickBtnNext: {
    // height: 50,
    // width: 50
  },
  wrapperBtnPrev: {
    // height: 40,
    // width: 70
  },
  WrapperScrollView: {},
  scrollView: {
    justifyContent: "center",
    height: hp("65%"),
    paddingLeft: 16,
    paddingRight: 16
  },
  transaction: {
    flexDirection: "row",
    flexWrap: "nowrap",
    alignItems: "center",
    width: "100%",
    height: 52,
    borderRadius: 6,
    padding: 8,
    marginTop: 8
  },
  transactionDate: {
    width: "25%",
    paddingLeft: 8,
    paddingRight: 8,
    color: colors.text.gray.icon,
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small
  },
  transactionNameAccount: {
    width: "45%",
    color: colors.text.gray.icon,
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.detail.small
  },
  transactionBalance: {
    paddingLeft: 8,
    paddingRight: 8,
    color: colors.text.gray.icon,
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small
  },
  buttonWrapper: {
    paddingLeft: getReponsive.width(61),
    paddingRight: getReponsive.width(61)
  }
});
export default styles;
