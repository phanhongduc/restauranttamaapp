import React, { Component } from "react";
import { Text, View, FlatList } from "react-native";
import { observer, inject } from "mobx-react";
import Swiper from "react-native-swiper";
import Icon from "react-native-vector-icons/MaterialIcons";
import HeaderLeft from "./../../../../components/HeaderLeft";
import styles from "./styles";
import { fonts, colors, fontSizes } from "../../../../assets/constants";

@inject("historyScreenStore")
@observer
export default class HistoryScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Historique",
      headerLeft: <HeaderLeft goBack={() => navigation.goBack()} />
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      activeSlide: 0,
      loading: false,
      isRefreshing: false
    };
  }

  componentDidMount() {
    const { historyScreenStore } = this.props;
    historyScreenStore.getInformation();
  }

  _nextBtn = () => {
    return <Icon name="chevron-right" size={32} color="#595A5A" />;
  };
  _preBtn = () => {
    return <Icon name="chevron-left" size={32} color="#595A5A" />;
  };
  _checkIndexIsEven = n => {
    return n % 2 == 0;
  };

  _renderItem = ({ item, index }) => {
    let date = item.date.slice(5, 10);
    let month = +item.date.slice(5, 7);

    const day = item.date.slice(8, 10);
    date = day + " " + this._transferMonthToWord(month);
    return (
      <View
        style={[
          styles.transaction,
          {
            backgroundColor: this._checkIndexIsEven(index)
              ? colors.background.gray.item
              : "white"
          }
        ]}
      >
        <Text style={styles.transactionDate}>{date}</Text>
        <Text style={styles.transactionNameAccount}>{item.name}</Text>
        <Text style={styles.transactionBalance}>+{item.balance} FCP</Text>
      </View>
    );
  };

  _handleSwipeIndexChange = activeSlide => {
    this.setState({ activeSlide }, console.log(this.state.activeSlide));
  };

  _transferMonthToWord = m => {
    var month = new Array();
    month[0] = "Jan";
    month[1] = "Fév";
    month[2] = "Mar";
    month[3] = "Avr";
    month[4] = "Mai";
    month[5] = "Jui";
    month[6] = "Jui";
    month[7] = "Aoû";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Déc";

    var d = new Date();
    var n = month[m - 1];
    return n;
  };

  render() {
    const { historyScreenStore } = this.props;
    const d = new Date();
    let m = d.getMonth();

    m = parseInt(m) + 1;
    if (m < 10) {
      m = "0" + m;
    }
    let lm = +m - 1;
    let l2m = +m - 2;
    if (lm < 10) {
      lm = "0" + lm;
    }
    if (l2m < 10) {
      l2m = "0" + l2m;
    }
    y = d.getFullYear();
    const currentMonth = this._transferMonthToWord(m) + " " + y;
    const lastMonth = this._transferMonthToWord(lm) + " " + y;
    const last2Months = this._transferMonthToWord(l2m) + " " + y;
    console.log(currentMonth);
    let dataMonthTransactionsRenderCurrent = historyScreenStore.dataTransactionCurrentMonth
      ? historyScreenStore.dataTransactionCurrentMonth
      : null;
    let dataMonthTransactionsRenderLastMonth = historyScreenStore.dataTransactionLastMonth
      ? historyScreenStore.dataTransactionLastMonth
      : null;
    let dataMonthTransactionsRenderLast2Months = historyScreenStore.dataTransactionLast2Months
      ? historyScreenStore.dataTransactionLast2Months
      : null;
    let dataRender;
    if (this.state.activeSlide == 2) {
      dataRender = dataMonthTransactionsRenderCurrent;
    } else if (this.state.activeSlide == 1) {
      dataRender = dataMonthTransactionsRenderLastMonth;
    } else if (this.state.activeSlide == 0) {
      dataRender = dataMonthTransactionsRenderLast2Months;
    }
    console.log(dataMonthTransactionsRenderCurrent);

    return (
      <View
        style={{ flex: 1, alignItems: "center", backgroundColor: "#FBFCFC" }}
      >
        <View style={styles.wrapper}>
          <Swiper
            autoplay={false}
            // containerStyle={styles.swiper}
            // style={styles.child}
            showsButtons={true}
            loop={false}
            showsPagination={false}
            nextButton={this._nextBtn()}
            prevButton={this._preBtn()}
            buttonWrapperStyle={styles.buttonWrapper}
            onIndexChanged={this._handleSwipeIndexChange}
            index={this.state.activeSlide}
          >
            <View style={styles.slide}>
              <Text style={styles.text}>{last2Months}</Text>
            </View>
            <View style={styles.slide}>
              <Text style={styles.text}>{lastMonth}</Text>
            </View>
            <View style={styles.slide}>
              <Text style={styles.text}>{currentMonth}</Text>
            </View>
          </Swiper>
        </View>
        <View style={styles.scrollView}>
          {dataRender && (
            <FlatList
              data={dataRender}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          )}
        </View>
      </View>
    );
  }
}
