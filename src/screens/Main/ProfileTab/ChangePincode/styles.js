import { StyleSheet, Dimensions } from "react-native";
import { fontSizes, fonts, colors } from "../../../../assets/constants";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: colors.secondary
  },
  input: {
    height: "8%",
    width: "35%",
    maxHeight: 60,
    alignItems: "center"
  },
  titleContainer: {
    height: 50
  },
  titleWrapper: {
    alignItems: "center"
  },
  title: {
    color: colors.text.black,
    fontSize: fontSizes.title,
    fontFamily: fonts.primary.semibold
  },
  errorMessage: {
    width: width * 0.5,
    textAlign: "center",
    color: colors.text.red
  },
  pincodeTextContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  pincodeTextWrapper: {
    alignItems: "center",
    width: "80%",
    height: "100%"
  },
  pincodeText: {
    fontSize: 40,
    color: colors.text.black,
    fontFamily: fonts.primary.medium
  },
  underline: {
    height: 1,
    backgroundColor: colors.text.black,
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0
  },
  deleteIconWrapper: {
    height: "100%",
    paddingTop: 5,
    paddingLeft: 10
  },
  deleteIcon: {
    color: colors.text.gray.icon
  },
  disabledButton: {
    backgroundColor: colors.disabled
  }
});

export default styles;
