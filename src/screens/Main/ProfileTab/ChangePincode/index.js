import React, { Component } from "react";
import { View, Text, TouchableOpacity, BackHandler } from "react-native";
import { observer, inject } from "mobx-react";
import * as Animatable from "react-native-animatable";
import Icon from "react-native-vector-icons/Feather";
import changePincodeStore from "./../../../../stores/ChangePincodeStore";
import HeaderLeft from "./../../../../components/HeaderLeft";
import Keyboard from "./../../../../components/Keyboard";
// import Button from "./../../../../components/Button";
import styles from "./styles";

@inject("keyboardStore", "changePincodeStore")
@observer
export default class ChangePinCodeScreen extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Changez votre code PIN",
      headerLeft: (
        <HeaderLeft
          goBack={() => {
            if (
              changePincodeStore.numberFailed >=
              changePincodeStore.MAXIMUM_FAILURE
            ) {
              return;
            }
            switch (changePincodeStore.inputType) {
              case changePincodeStore.INPUT_NEW_PINCODE:
              case changePincodeStore.REINPUT_NEW_PINCODE:
                changePincodeStore.handleReInputCurrentPincode();
                break;
              case changePincodeStore.INPUT_CONFIRM_PINCODE:
                changePincodeStore.handleReInputNewPincode();
                break;
              default:
                navigation.pop();
                break;
            }
          }}
        />
      )
    };
  };

  componentDidMount() {
    const { changePincodeStore, navigation } = this.props;
    changePincodeStore.initScreen(navigation);
    navigation.setParams({ showTabBar: false });

    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleHardwareBack
    );
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  handleOnDelete = () => {
    const { changePincodeStore, keyboardStore } = this.props;
    changePincodeStore.removePincode();
    keyboardStore.onDelete();
  };

  handleHardwareBack = () => {
    const { changePincodeStore } = this.props;
    if (
      changePincodeStore.numberFailed === changePincodeStore.MAXIMUM_FAILURE
    ) {
      return;
    }
    switch (changePincodeStore.inputType) {
      case changePincodeStore.INPUT_NEW_PINCODE:
      case changePincodeStore.REINPUT_NEW_PINCODE:
        changePincodeStore.handleReInputCurrentPincode();
        return true;
      case changePincodeStore.INPUT_CONFIRM_PINCODE:
        changePincodeStore.handleReInputNewPincode();
        return true;
      default:
        return false;
    }
  };

  handleOnValidate = () => {};

  renderTitle = () => {
    const { changePincodeStore } = this.props;

    switch (changePincodeStore.inputType) {
      case changePincodeStore.INPUT_CURRENT_PINCODE:
        return (
          <Animatable.View
            ref={changePincodeStore.setTitleRef}
            style={styles.titleWrapper}
            useNativeDriver
          >
            <Text style={styles.title}>ENTREZ VOTRE</Text>
            <Text style={styles.title}>CODE PIN ACTUEL</Text>
          </Animatable.View>
        );
      case changePincodeStore.INPUT_NEW_PINCODE:
        return (
          <Animatable.View
            ref={changePincodeStore.setTitleRef}
            style={styles.titleWrapper}
            animation="bounceIn"
            duration={500}
            useNativeDriver
          >
            <Text style={styles.title}>ENTREZ VOTRE</Text>
            <Text style={styles.title}>NOUVEAU CODE TAMA'A</Text>
          </Animatable.View>
        );
      case changePincodeStore.INPUT_CONFIRM_PINCODE:
        return (
          <Animatable.View
            ref={changePincodeStore.setTitleRef}
            style={styles.titleWrapper}
            animation="bounceInRight"
            duration={900}
            useNativeDriver
          >
            <Text style={styles.title}>CONFIRMEZ</Text>
            <Text style={styles.title}>VOTRE NOUVEAU CODE PIN</Text>
          </Animatable.View>
        );
      case changePincodeStore.REINPUT_CURRENT_PINCODE:
        return (
          <Animatable.View
            ref={changePincodeStore.setTitleRef}
            style={styles.titleWrapper}
            animation="bounceInLeft"
            duration={900}
            useNativeDriver
          >
            <Text style={styles.title}>ENTREZ VOTRE</Text>
            <Text style={styles.title}>CODE PIN ACTUEL</Text>
          </Animatable.View>
        );
      case changePincodeStore.REINPUT_NEW_PINCODE:
        return (
          <Animatable.View
            ref={changePincodeStore.setTitleRef}
            style={styles.titleWrapper}
            animation="bounceIn"
            duration={900}
            useNativeDriver
          >
            <Text style={styles.title}>ENTREZ VOTRE</Text>
            <Text style={styles.title}>NOUVEAU CODE PIN</Text>
          </Animatable.View>
        );
    }
  };

  render() {
    const { changePincodeStore, keyboardStore } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          {!changePincodeStore.displayError ? (
            this.renderTitle()
          ) : (
            <Text style={[styles.title, styles.errorMessage]}>
              {changePincodeStore.errorMessage}
            </Text>
          )}
        </View>
        <View style={styles.input}>
          {keyboardStore.length ? (
            <View style={styles.pincodeTextContainer}>
              <View style={styles.pincodeTextWrapper}>
                <Text style={styles.pincodeText}>
                  {changePincodeStore.pincodeViewStr}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.deleteIconWrapper}
                onPress={this.handleOnDelete}
              >
                <Icon name="delete" size={25} style={styles.deleteIcon} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.underline} />
          )}
        </View>
        <Keyboard onPress={changePincodeStore.addPincode} />
        {/* <Button
          title="Valider"
          disabled={changePincodeStore.disableButton}
          style={changePincodeStore.disableButton && styles.disabledButton}
        /> */}
      </View>
    );
  }
}
