import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { inject, observer } from "mobx-react";
import HeaderLeft from "./../../../../components/HeaderLeft";
import styles from "./styles";

// sudo get restaurant info
@inject("restaurantStore")
@observer
export default class MesInfosScreen extends Component {
  constructor(props) {
    super(props);

    // sudo restaurant info
    this.state = {
      restaurant: null
    };
  }

  // sudo get restaurant
  async getRestaurant() {
    const { restaurantStore } = this.props;
    await restaurantStore.fetchListRestaurants();
    return restaurantStore.listRestaurants[0];
  }

  async componentDidMount() {
    let restaurant = await this.getRestaurant();
    this.setState({ restaurant: restaurant });
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Mes infos",
      headerLeft: <HeaderLeft goBack={() => navigation.goBack()} />
    };
  };

  render() {
    // sudo get restaurant
    let restaurant = {
      ...this.state.restaurant
    };
    if (!this.state.restaurant) return null;
    restaurant = {
      ...restaurant,
      icon: !!restaurant.images ? restaurant.images[0] : "",
      phoneNumber: [restaurant.phoneNumber1, restaurant.phoneNumber2]
    };

    return (
      <View style={styles.container}>
        <View>
          <Image
            source={{ uri: restaurant.icon }}
            style={styles.imageStyle}
            resizeMode={"contain"}
          />
          <View style={styles.wraperTitle}>
            <Text style={styles.nameRestaurant}>
              {restaurant.restaurantName}
            </Text>
            {/* <Text style={styles.infoRestaurant}>{restaurant.address}</Text> */}
          </View>
        </View>
        <View style={styles.wraperInfos}>
          <Text style={styles.info}>{restaurant.email}</Text>
          <Text style={styles.info}>{restaurant.phoneNumber1}</Text>
        </View>
        <View style={styles.contentContainer}>
          <Text style={styles.instructionText}>
            Pour modifier vos informations,
          </Text>
          <Text style={styles.instructionText}>
            connectez vous à la plateforme
          </Text>
        </View>
      </View>
    );
  }
}
