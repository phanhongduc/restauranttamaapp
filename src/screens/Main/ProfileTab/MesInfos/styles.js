import { StyleSheet, Dimensions } from "react-native";
import { fonts, colors, fontSizes } from "./../../../../assets/constants";
import { Header } from "react-navigation";
const { width, height } = Dimensions.get("window");
const mainScreen = height - Header.HEIGHT - 100;

const styles = StyleSheet.create({
  container: {
    height: mainScreen,
    alignItems: "center",
    justifyContent: "center"
  },
  imageStyle: {
    width: 159,
    height: 159
  },
  contentContainer: {
    alignItems: "center",
    marginTop: "5%",
    justifyContent: "center"
  },
  instructionText: {
    textAlign: "center",
    color: colors.text.black,
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.regular
  },
  wraperTitle: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 24,
    paddingBottom: 12
  },
  nameRestaurant: {
    color: colors.text.black,
    fontSize: fontSizes.detail.largerTitle,
    fontFamily: fonts.primary.bold
  },
  infoRestaurant: {
    color: colors.text.black,
    fontSize: fontSizes.detail.big,
    fontFamily: fonts.primary.regular
  },
  wraperInfos: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 12,
    paddingBottom: 24
  },
  info: {
    color: colors.text.black,
    fontSize: fontSizes.detail.big2,
    fontFamily: fonts.primary.semibold
  }
});

export default styles;
