import { StyleSheet, Dimensions } from "react-native";
import { Header } from "react-navigation";
import {
  fonts,
  colors,
  fontSizes,
  getReponsive
} from "./../../../../assets/constants";

const { height, width } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    height: mainScreenHeight - 100,
    width: width
  },
  imageStyle: {
    width: getReponsive.width(221),
    height: getReponsive.width(221)
  },
  contentContainer: {
    alignItems: "center",
    marginTop: "5%",
    justifyContent: "center"
  },
  instructionText: {
    textAlign: "center",
    color: colors.text.gray.instruction,
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.regular
  }
});

export default styles;
