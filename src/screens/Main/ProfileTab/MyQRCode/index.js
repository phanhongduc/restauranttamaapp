import React, { Component } from "react";
import { View, Text } from "react-native";
import FastImage from "react-native-fast-image";
import { observer, inject } from "mobx-react";
import HeaderLeft from "./../../../../components/HeaderLeft";
import styles from "./styles";

@observer
export default class MyQRCodeScreen extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Mon QR code personnel",
      headerLeft: <HeaderLeft goBack={() => navigation.goBack()} />
    };
  };

  render() {
    return (
      <View style={styles.container}>
        <FastImage
          source={{
            uri:
              "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/QR_code_for_mobile_English_Wikipedia.svg/220px-QR_code_for_mobile_English_Wikipedia.svg.png"
          }}
          style={styles.imageStyle}
        />
        <View style={styles.contentContainer}>
          <Text style={styles.instructionText}>À présenter aux clients</Text>
          <Text style={styles.instructionText}>
            disposant de l'application pour
          </Text>
          <Text style={styles.instructionText}>
            payer avec leurs PASS TAMA'A
          </Text>
        </View>
      </View>
    );
  }
}
