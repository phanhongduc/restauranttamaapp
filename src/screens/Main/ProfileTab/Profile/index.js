import React, { Component } from "react";
import { View, Text, TouchableOpacity, Dimensions } from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import { observer } from "mobx-react";
import Button from "./../../../../components/Button";
import profileStore from "./../../../../stores/ProfileStore";
import { colors } from "./../../../../assets/constants";
import styles from "./styles";

@observer
export default class ProfileScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "Mon profil",
    headerRight: (
      <TouchableOpacity
        style={{ padding: 10 }}
        onPress={() => navigation.navigate("Setting")}
      >
        <Icon name="setting" size={25} color={colors.text.white} />
      </TouchableOpacity>
    )
  });

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    profileStore.getContent();
  }

  render() {
    var { width } = Dimensions.get("window");
    // var isMobile = true;
    if (width > 500) {
      isMobile = false;
    } else {
      isMobile = true;
    }
    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <Text style={styles.titleText}>Solde global disponible :</Text>
          <Text style={styles.price}>{profileStore.content.age} FCP</Text>
          <View style={[styles.triangle, styles.arrowUp]} />
          <View style={styles.balance}>
            <Text style={styles.balance_text}>Dépensable aujourd'hui</Text>
            <Text style={styles.balance_price}>
              {profileStore.content.balance} FCP
            </Text>
          </View>
          <View style={styles.information}>
            <Text style={styles.information_title}>
              Conditions d’utilisation
            </Text>
            <Text style={styles.information_item}>
              Jours: {profileStore.content.name}
            </Text>
            <Text style={styles.information_item}>
              Plages horaire: De 11:00 à 14:00
            </Text>
            <Text style={styles.information_item}>
              Plafond journalier: 1 000 XPF
            </Text>
            <Text style={styles.information_item}>
              Date d’expiration: XX/XX/XX
            </Text>
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            labelStyle={styles.btnText}
            style={{ alignItems: "center" }}
            onPress={() => this.props.navigation.navigate("MyQRCode")}
            title={"Mon QR Code"}
          />
        </View>
      </View>
    );
  }
}
