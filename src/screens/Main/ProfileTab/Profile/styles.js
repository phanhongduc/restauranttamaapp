import { StyleSheet, Dimensions } from "react-native";
import { Header } from "react-navigation";
import {
  fonts,
  colors,
  fontSizes,
  backgroundColors
} from "./../../../../assets/constants";

const { height, width } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  contentContainer: {
    height: mainScreenHeight * 0.62,
    width: width,
    alignItems: "center"
  },
  buttonContainer: {
    height: mainScreenHeight * 0.38 - 100,
    width: width,
    justifyContent: "center",
    alignItems: "center",
    borderTopWidth: 1,
    borderTopColor: colors.text.gray.line,
  },
  titleText: {
    color: colors.primary.orange,
    fontFamily: fonts.primary.bold,
    fontSize: fontSizes.detail.largerTitle,
    paddingTop: 24
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: "transparent",
    borderStyle: "solid"
  },
  arrowUp: {
    borderTopWidth: 0,
    borderRightWidth: 12,
    borderBottomWidth: 12,
    borderLeftWidth: 12,
    borderTopColor: "transparent",
    borderRightColor: "transparent",
    borderBottomColor: backgroundColors.balance,
    borderLeftColor: "transparent"
  },
  price: {
    paddingTop: 8,
    paddingBottom: 16,
    fontSize: fontSizes.balance,
    color: colors.text.blackGreen,
    fontFamily: fonts.primary.light
  },
  balance: {
    backgroundColor: backgroundColors.balance,
    height: 82,
    width: 219,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 16
  },
  balance_text: {
    fontFamily: fonts.primary.medium,
    color: colors.text.gray.instruction,
    fontSize: fontSizes.detail.small,
    paddingBottom: 4
  },
  balance_price: {
    fontSize: fontSizes.detail.bigger,
    fontFamily: fonts.primary.semibold,
    color: colors.text.blackGreen
  },
  information: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 32,
    paddingBottom: 48
  },
  information_title: {
    fontFamily: fonts.primary.bold,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.instruction
  },
  information_item: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.instruction
  },
  btnText: {
    fontSize: fontSizes.detail.huge
  }
});

export default styles;
