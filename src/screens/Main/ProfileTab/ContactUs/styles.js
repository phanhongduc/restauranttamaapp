import { StyleSheet, Dimensions } from "react-native";
import { Header } from "react-navigation";
import { fonts, colors, fontSizes } from "./../../../../assets/constants";

const { height } = Dimensions.get("window");
const mainScreenHeight = height - Header.HEIGHT;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  infoWrapper: {
    marginVertical: mainScreenHeight * 0.1
  },
  info: {
    textAlign: "center",
    color: colors.text.gray.title,
    fontSize: fontSizes.title,
    fontFamily: fonts.primary.semibold
  }
});

export default styles;
