import { StyleSheet, Dimensions } from "react-native";
import {
  fontSizes,
  fonts,
  colors,
  getReponsive
} from "./../../../assets/constants";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: colors.secondary
  },
  input: {
    height: getReponsive.height(55),
    alignItems: "center"
  },
  titleContainer: {
    marginTop: getReponsive.height(54)
  },
  titleWrapper: {
    alignItems: "center"
  },
  title: {
    color: colors.text.black,
    fontSize: fontSizes.title,
    fontFamily: fonts.primary.semibold
  },
  errorMessage: {
    textAlign: "center",
    color: colors.text.red
  },
  pincodeTextContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  pincodeTextWrapper: {
    alignItems: "center",
    width: getReponsive.width(171),
    height: getReponsive.height(55)
  },
  pincodeText: {
    fontSize: getReponsive.fontSize(45),
    color: colors.text.black,
    fontFamily: fonts.primary.medium
  },
  underline: {
    height: getReponsive.height(55),
    width: getReponsive.width(219),
    borderBottomWidth: getReponsive.borderWidth(1),
    borderColor: colors.text.black
  },
  deleteIconWrapper: {
    justifyContent: "center"
  },
  deleteIcon: {
    color: colors.text.gray.icon
  },
  disabledButton: {
    backgroundColor: colors.disabled
  }
});

export default styles;
