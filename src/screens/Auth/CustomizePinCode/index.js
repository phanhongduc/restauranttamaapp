import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { observer, inject } from "mobx-react";
import * as Animatable from "react-native-animatable";
import Icon from "react-native-vector-icons/Feather";
import customizePinCodeStore from "./../../../stores/CustomizePinCodeStore";
import Keyboard from "./../../../components/Keyboard";
import Button from "./../../../components/Button";
import HeaderLeft from "./../../../components/HeaderLeft";
import styles from "./styles";

@inject("keyboardStore", "customizePinCodeStore", "etape1Store")
@observer
export default class CustomizePinCode extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Personnalisez votre code Tama'a",
      headerLeft: (
        <HeaderLeft
          goBack={() => {
            if (
              customizePinCodeStore.numberFailed >=
              customizePinCodeStore.MAXIMUM_FAILURE
            ) {
              return;
            } else if (
              customizePinCodeStore.inputType ===
              customizePinCodeStore.INPUT_CONFIRM_PINCODE
            ) {
              customizePinCodeStore.handleReInputPincode();
            } else {
              navigation.pop();
            }
          }}
        />
      )
    };
  };

  componentDidMount() {
    const { customizePinCodeStore, navigation } = this.props;
    customizePinCodeStore.initScreen(navigation);
  }

  handleOnDelete = () => {
    this.props.customizePinCodeStore.removePincode();
    this.props.keyboardStore.onDelete();
  };

  handleOnValidate = () => {};

  renderTitle = () => {
    const { customizePinCodeStore } = this.props;

    switch (customizePinCodeStore.inputType) {
      case customizePinCodeStore.INPUT_PINCODE:
        return (
          <Animatable.Text
            ref={customizePinCodeStore.setTitleRef}
            style={styles.title}
            useNativeDriver
          >
            ENTREZ VOTRE CODE TAMA'A
          </Animatable.Text>
        );
      case customizePinCodeStore.INPUT_CONFIRM_PINCODE:
        return (
          <Animatable.Text
            ref={customizePinCodeStore.setTitleRef}
            animation="bounceInRight"
            duration={900}
            style={styles.title}
            useNativeDriver
          >
            CONFIRMEZ VOTRE CODE TAMA'A
          </Animatable.Text>
        );
      case customizePinCodeStore.REINPUT_PINCODE:
        return (
          <Animatable.Text
            ref={customizePinCodeStore.setTitleRef}
            animation="bounceInLeft"
            duration={900}
            style={styles.title}
            useNativeDriver
          >
            ENTREZ LVOTRE CODE TAMA'A
          </Animatable.Text>
        );
    }
  };

  render() {
    const { customizePinCodeStore, keyboardStore } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          {!customizePinCodeStore.displayError ? (
            this.renderTitle()
          ) : (
            <Text style={[styles.title, styles.errorMessage]}>
              {customizePinCodeStore.errorMessage}
            </Text>
          )}
        </View>
        <View style={styles.input}>
          {keyboardStore.length ? (
            <View style={styles.pincodeTextContainer}>
              <View style={styles.pincodeTextWrapper}>
                <Text style={styles.pincodeText}>
                  {customizePinCodeStore.pincodeViewStr}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.deleteIconWrapper}
                onPress={this.handleOnDelete}
              >
                <Icon name="delete" size={25} style={styles.deleteIcon} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.underline} />
          )}
        </View>
        <Keyboard onPress={customizePinCodeStore.addPincode} />
        {/* <Button
          title="Valider"
          disabled={customizePinCodeStore.disableButton}
          style={
            customizePinCodeStore.disableButton ? styles.disabledButton : null
          }
          onPress={customizePinCodeStore.handleOnValidate}
        /> */}
      </View>
    );
  }
}
