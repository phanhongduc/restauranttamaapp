import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { observer } from "mobx-react";
import Button from "./../../../components/Button";
import styles from "./styles";

@observer
export default class ConfirmedScreen extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
  }

  render() {
    const instruction =
      "Vous avez reçu un email\navec un lien de réinitialisation";
    return (
      <View style={styles.container}>
        <Image
          style={styles.logo}
          source={require("./../../../assets/ic_logo.png")}
          resizeMode="stretch"
        />
        <View style={styles.wrapper}>
          <View style={styles.textWrapper}>
            <Text style={styles.title}>Email envoyé</Text>
            <Text style={styles.instruction}>{instruction}</Text>
          </View>
          <Button
            style={styles.button}
            labelStyle={styles.buttonText}
            title="Connexion"
            onPress={() => this.props.navigation.navigate("SignIn")}
          />
        </View>
      </View>
    );
  }
}
