import { StyleSheet } from "react-native";
import {
  fonts,
  colors,
  fontSizes,
  getReponsive
} from "../../../assets/constants";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background.orange
  },
  button: {
    backgroundColor: colors.secondary,
    marginTop: getReponsive.height(83),
    height: getReponsive.height(52),
    width: getReponsive.width(180),
    borderRadius: getReponsive.borderRadius(26)
  },
  logo: {
    marginLeft: getReponsive.width(103),
    marginTop: getReponsive.height(163),
    height: getReponsive.height(170),
    width: getReponsive.width(168)
  },
  buttonText: {
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.button,
    color: colors.text.orange
  },
  wrapper: {
    alignItems: "center"
  },
  textWrapper: {
    marginTop: getReponsive.height(42)
  },
  title: {
    // height: getReponsive.height(24),
    fontFamily: fonts.primary.regular,
    color: colors.text.white.normal,
    fontSize: fontSizes.detail.largeTitle,
    textAlign: "center"
  },
  instruction: {
    marginTop: getReponsive.height(57),
    fontFamily: fonts.primary.regular,
    color: colors.text.white.normal,
    fontSize: fontSizes.detail.big2,
    textAlign: "center"
  }
});

export default styles;
