import { StyleSheet, Dimensions, Platform } from "react-native";
import {
  fonts,
  colors,
  fontSizes,
  getReponsive
} from "../../../assets/constants";

const screen = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    marginTop: getReponsive.height(94)
  },
  paginationContainer: {
    backgroundColor: colors.background.white
    // marginTop: 10,
    // marginBottom: 10,
  },
  button: {
    width: getReponsive.width(285),
    height: getReponsive.height(52),
    backgroundColor: colors.background.orange,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: getReponsive.borderRadius(26)
    // marginTop: 10,
    // marginBottom: 10
  },
  image: {
    height: getReponsive.height(230),
    width: getReponsive.width(271)
  },
  buttonText: {
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.detail.huge,
    color: colors.text.white.normal
  },
  blankArea: {
    width: "90%",
    height: 50
  },
  dotStyle: {
    width: getReponsive.height(12),
    height: getReponsive.height(12),
    borderRadius: getReponsive.height(6),
    // marginHorizontal: 8,
    backgroundColor: colors.background.orange
  },
  inactiveDotStyle: {
    backgroundColor: colors.background.gray.dot,
    width: getReponsive.height(12),
    height: getReponsive.height(12),
    borderRadius: getReponsive.height(6)
    // marginHorizontal: 8
  },
  itemContainer: {
    alignItems: "center"
  },
  itemTitle: {
    fontFamily: fonts.primary.bold,
    color: colors.text.black,
    fontSize: fontSizes.bigtitle,
    textAlign: "center"
  },
  imageWrapper: {
    marginTop: getReponsive.height(42)
  },
  itemInstruction: {
    marginTop: getReponsive.height(35),
    fontSize: fontSizes.detail.huge,
    textAlign: "center",
    fontFamily: fonts.primary.semibold,
    color: colors.text.black
  }
});
export default styles;
