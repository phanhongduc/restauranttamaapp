import React, { Component } from "react";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { View, Text, Image, Dimensions, TouchableOpacity } from "react-native";
import { observer } from "mobx-react";
import styles from "./styles";
import { colors } from "../../../assets/constants";

const screen = Dimensions.get("window");

// sudo information
const information = [
  {
    title: `Bienvenue sur \nI'appli Pass Tama'a`,
    content: `Votre système de paiement \nen tires restaurants \ndématérialiés`,
    image: require(`../../../assets/ig_aboutapp_1.png`)
  },
  {
    title: `Attirez les clients \nPass Tama'a`,
    content: `Diffusez vos offres et photos \nalléchantes de vos menus`,
    image: require(`../../../assets/ig_aboutapp_2.png`)
  },
  {
    title: `Bénéficiez d'un \nsystème de paiement`,
    content: `Prélevez au franc près \nles adhérents du réseau \nPass Tama'a`,
    image: require(`../../../assets/ig_aboutapp_3.png`)
  },
  {
    title: `Suivez vos \nrevenus`,
    content: `Consultez I'historique \ndes transactions dans \nvotre espace sécurisé`,
    image: require(`../../../assets/ig_aboutapp_4.png`)
  }
];

@observer
export default class Etape1Screen extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      activeSlide: 0
    };
  }

  handleOnNavigate = () => {
    this.props.navigation.navigate("CustomizePinCode");
  };

  setActiveSlide = index => {
    this.setState({ activeSlide: index });
  };

  _renderItem({ item, index }) {
    return (
      <View key={"aboutapp_" + index} style={styles.itemContainer}>
        <Text style={styles.itemTitle}>{item.title}</Text>
        <View style={styles.imageWrapper}>
          <Image
            style={styles.image}
            source={item.image}
            resizeMode={"stretch"}
          />
        </View>
        <Text style={styles.itemInstruction}>{item.content}</Text>
      </View>
    );
  }

  get pagination() {
    return (
      <Pagination
        dotsLength={information.length}
        activeDotIndex={this.state.activeSlide}
        containerStyle={styles.paginationContainer}
        dotStyle={styles.dotStyle}
        inactiveDotStyle={styles.inactiveDotStyle}
        inactiveDotOpacity={0.7}
        inactiveDotScale={1}
      />
    );
  }

  render() {
    return (
      <View style={[styles.container]}>
        <Carousel
          ref={ref => (this.carousel = ref)}
          data={information}
          renderItem={this._renderItem}
          onSnapToItem={index => this.setActiveSlide(index)}
          sliderWidth={screen.width}
          itemWidth={screen.width}
          removeClippedSubviews={false}
        />
        <View style={styles.paginationContainer}>{this.pagination}</View>
        {this.state.activeSlide === information.length - 1 ? (
          <TouchableOpacity
            style={styles.button}
            onPress={this.handleOnNavigate}
          >
            <Text style={styles.buttonText}>Personnaliser votre code</Text>
          </TouchableOpacity>
        ) : (
          <View style={styles.blankArea} />
        )}
      </View>
    );
  }
}
