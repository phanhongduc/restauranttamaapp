import { StyleSheet } from "react-native";
import {
  fonts,
  colors,
  fontSizes,
  getReponsive
} from "../../../assets/constants";

const styles = StyleSheet.create({
  infoScreenWrapper: {
    flex: 1
  },
  ScrollViewWrapper: {
    marginLeft: getReponsive.width(33),
    marginTop: getReponsive.width(45),
    width: getReponsive.width(307),
    height: getReponsive.height(592)
  },
  btnRefuser: {
    position: "absolute",
    left: getReponsive.width(33),
    top: getReponsive.height(584),
    height: getReponsive.height(52),
    width: getReponsive.width(128),
    borderRadius: getReponsive.borderRadius(26),
    backgroundColor: colors.primary.white
  },
  emptyFooter: {
    height: getReponsive.height(74),
    backgroundColor: colors.secondary
  },
  absoluteFooter: {
    position: "absolute",
    height: getReponsive.height(96),
    top: getReponsive.height(584),
    left: getReponsive.width(33),
    width: getReponsive.width(283)
  },
  btnDisabled: {
    position: "absolute",
    left: getReponsive.width(174),
    top: getReponsive.height(584),
    height: getReponsive.height(52),
    width: getReponsive.width(128),
    borderRadius: getReponsive.borderRadius(26),
    backgroundColor: colors.primary.white
  },
  btnGreen: {
    backgroundColor: colors.primary.orange
  },
  btnTextDisabled: {
    color: "#E3D3D0",
    textDecorationLine: "none"
  },
  btnText: {
    fontFamily: fonts.secondary.medium,
    color: colors.text.gray.buttonRefulser,
    textDecorationLine: "underline",
    fontSize: fontSizes.detail.big
  },
  btnTextGreen: {
    color: colors.text.white.normal,
    textDecorationLine: "none",
    fontSize: fontSizes.detail.huge
  },
  btnView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 24
  }
});

export const markdownStyle = StyleSheet.create({
  text: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.instruction,
    width: getReponsive.width(283)
    // width: getReponsive.width(307)
  }
});

export default styles;
