import React, { Component } from "react";
import { View, ScrollView, Dimensions } from "react-native";
import Markdown from "react-native-markdown-renderer";
import { observer, inject } from "mobx-react";
import { Header } from "react-navigation";
import Button from "../../../components/Button";
import styles, { markdownStyle } from "./styles";
import termConditionStore from "../../../stores/TermsConditionStore";

const { height } = Dimensions.get("window");

@inject(`termConditionStore`)
@observer
export default class TermConditionScreen extends Component {
  static navigationOptions = {
    title: "Conditions générales",
    headerLeft: null
  };

  constructor(props) {
    super(props);
    this.state = {
      disableValidateBtn: true
    };
  }

  async componentDidMount() {
    await termConditionStore.getContent();
    // await this.props.termConditionStore.getContent();
  }

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;

    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  onContentSizeChange = (contentWidth, contentHeight) => {
    if (!contentWidth) return;
    const mainHeight = height - Header.HEIGHT - 100;
    if (contentHeight < mainHeight) {
      this.setState({
        disableValidateBtn: false
      });
    }
  };

  renderEmptyFooter(style = {}) {
    return <View style={[styles.emptyFooter, style]} />;
  }

  render() {
    const { disableValidateBtn } = this.state;
    // const { termConditionStore } = this.props;
    return (
      <View style={styles.infoScreenWrapper}>
        <View style={styles.ScrollViewWrapper}>
          <ScrollView
            onScroll={({ nativeEvent }) => {
              if (this.isCloseToBottom(nativeEvent)) {
                this.setState({ disableValidateBtn: false });
              } else {
                this.setState({ disableValidateBtn: true });
              }
            }}
            scrollEventThrottle={100}
            onContentSizeChange={this.onContentSizeChange}
          >
            <Markdown style={markdownStyle}>
              {!termConditionStore.isLoading
                ? termConditionStore.content.content
                : ""}
            </Markdown>
            {this.renderEmptyFooter({})}
          </ScrollView>
        </View>
        {this.renderEmptyFooter(styles.absoluteFooter)}
        <Button
          disabled={disableValidateBtn}
          style={styles.btnRefuser}
          labelStyle={
            disableValidateBtn == true
              ? [styles.btnText, styles.btnTextDisabled]
              : styles.btnText
          }
          onPress={() => this.props.navigation.goBack()}
          title={"Refuser"}
        />
        <Button
          disabled={disableValidateBtn}
          style={
            disableValidateBtn == false
              ? [styles.btnDisabled, styles.btnGreen]
              : styles.btnDisabled
          }
          labelStyle={
            disableValidateBtn == true
              ? [styles.btnText, styles.btnTextDisabled]
              : [styles.btnText, styles.btnTextGreen]
          }
          onPress={() => this.props.navigation.navigate("Etape1")}
          title={"Accepter"}
        />
      </View>
    );
  }
}
