import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, TextInput } from "react-native";
import IconIonic from "react-native-vector-icons/Ionicons";
import IconFontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { observer } from "mobx-react";
import Button from "./../../../components/Button";
import DismissKeyboard from "./../../../components/DismissKeyboard";
import { fonts, colors, fontSizes } from "../../../assets/constants";
import styles from "./styles";

@observer
export default class SignInScreen extends Component {
  static navigationOptions = {
    header: null,
    headerBackTitle: null
  };
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  render() {
    return (
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ backgroundColor: colors.background.orange }}
      >
        <DismissKeyboard>
          <View style={styles.container}>
            <Image
              style={styles.logo}
              resizeMode="stretch"
              source={require("./../../../assets/ic_logo.png")}
            />
            <View style={styles.inputContainer}>
              <View style={styles.inputEmailWrapper}>
                <IconFontAwesome5
                  name="user-alt"
                  size={18}
                  style={styles.icon}
                />
                <TextInput
                  value={this.state.email}
                  adjustsFontSizeToFit
                  placeholder={"Identifiant"}
                  placeholderTextColor={colors.text.white.placeholder}
                  onChangeText={text => this.setState({ email: text })}
                  style={styles.textInput}
                  autoCorrect={false}
                  autoCapitalize="none"
                  underlineColorAndroid="transparent"
                  returnKeyType="next"
                  onSubmitEditing={() => this.passwordInputRef.focus()}
                />
              </View>
              <View style={styles.inputPasswordWrapper}>
                <IconIonic name="ios-lock" size={25} style={styles.icon} />
                <TextInput
                  value={this.state.password}
                  adjustsFontSizeToFit
                  secureTextEntry={true}
                  placeholder={"Mot de Passe"}
                  placeholderTextColor={colors.text.white.placeholder}
                  onChangeText={text => this.setState({ password: text })}
                  style={styles.textInput}
                  autoCorrect={false}
                  autoCapitalize="none"
                  underlineColorAndroid="transparent"
                  returnKeyType="done"
                  autoCompleteType="email"
                  ref={ref => (this.passwordInputRef = ref)}
                />
              </View>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Forgot")}
              >
                <Text adjustsFontSizeToFit style={styles.forgotPasswordWrapper}>
                  Mot de passe oublié?
                </Text>
              </TouchableOpacity>
              <Button
                style={styles.button}
                labelStyle={styles.buttonText}
                title="Connexion"
                onPress={() => this.props.navigation.navigate("Terms")}
              />
            </View>
          </View>
        </DismissKeyboard>
      </KeyboardAwareScrollView>
    );
  }
}
