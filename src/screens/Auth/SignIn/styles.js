import { StyleSheet, Dimensions, StatusBar, Platform } from "react-native";
import {
  fonts,
  colors,
  fontSizes,
  getReponsive
} from "../../../assets/constants";
let { height, width } = Dimensions.get("window");
const statusBarHeight = Platform.OS === "android" ? StatusBar.currentHeight : 0;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // alignItems: "center",
    // justifyContent: "center",
    backgroundColor: colors.primary.orange
    // height: getReponsiveSize(height - statusBarHeight),
    // height: height
  },
  logo: {
    marginLeft: getReponsive.width(103),
    marginTop: getReponsive.height(163),
    height: getReponsive.height(170),
    width: getReponsive.width(168)
  },
  inputContainer: {
    // paddingTop: 20,
    // paddingBottom: 20,
    marginTop: getReponsive.height(64.5),
    alignItems: "center"
  },
  inputEmailWrapper: {
    // width: "80%",
    // height: 60,
    width: getReponsive.width(282),
    height: getReponsive.height(52),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    borderWidth: getReponsive.borderWidth(2),
    borderColor: colors.secondary,
    borderRadius: getReponsive.borderRadius(26)
    // marginTop: 70
    // marginBottom: 10
  },
  inputPasswordWrapper: {
    // width: "80%",
    // height: 60,
    width: getReponsive.width(282),
    height: getReponsive.height(52),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    borderWidth: getReponsive.borderWidth(2),
    borderColor: colors.secondary,
    borderRadius: getReponsive.borderRadius(26),
    // marginTop: 10,
    // marginBottom: 10
    marginTop: getReponsive.height(22)
  },
  textInput: {
    width: "70%",
    // borderWidth: 1,
    // flex: 1,
    color: colors.text.white.normal,
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.button,
    // fontSize: 12,
    fontWeight: "400",
    padding: 0
  },
  forgotPasswordWrapper: {
    marginTop: getReponsive.height(6.5),
    color: colors.text.white.normal,
    textDecorationLine: "underline",
    fontSize: fontSizes.detail.small,
    fontFamily: fonts.primary.regular
  },
  button: {
    backgroundColor: colors.secondary,
    // marginTop: 50,
    marginTop: getReponsive.height(52),
    width: getReponsive.width(180),
    height: getReponsive.height(52)
    // borderWidth: 1
    // marginBottom: 0
  },
  buttonText: {
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.button,
    color: colors.text.orange
  },
  icon: {
    color: colors.text.white.normal
  }
});

export default styles;
