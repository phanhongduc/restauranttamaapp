import { StyleSheet, Dimensions, StatusBar, Platform } from "react-native";
import { Header } from "react-navigation";
import {
  fonts,
  colors,
  fontSizes,
  getReponsive
} from "../../../assets/constants";

const { height } = Dimensions.get("window");
const statusBarHeight = Platform.OS === "android" ? StatusBar.currentHeight : 0;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary.orange
    // height: height - Header.HEIGHT - statusBarHeight
  },
  buttonBack: {
    marginTop: getReponsive.height(33),
    marginLeft: getReponsive.width(41)
  },
  logo: {
    marginLeft: getReponsive.width(103),
    marginTop: getReponsive.height(17),
    height: getReponsive.height(170),
    width: getReponsive.width(168)
  },
  contentContainer: {
    marginTop: getReponsive.height(22),
    alignItems: "center"
  },
  titleWrapper: {
    marginBottom: getReponsive.height(42.5),
    alignItems: "center"
  },
  title: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.largeTitle,
    color: colors.text.white.normal,
    marginBottom: getReponsive.height(10)
  },
  subTitle: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.big2,
    color: colors.text.white.normal,
    textAlign: "center"
  },

  inputWrapper: {
    width: getReponsive.width(282),
    height: getReponsive.height(52),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    borderWidth: getReponsive.borderWidth(2),
    borderColor: colors.secondary,
    borderRadius: getReponsive.borderRadius(26),
    marginBottom: getReponsive.height(22)
  },

  textInput: {
    width: "70%",
    color: colors.text.white.normal,
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.button,
    padding: 0
  },
  errorContainer: {
    marginTop: getReponsive.height(9.5),
    marginBottom: getReponsive.height(38),
    height: getReponsive.height(26)
  },
  errorTitle: {
    fontFamily: fonts.primary.bold,
    fontSize: fontSizes.detail.small,
    color: colors.text.white.normal,
    textAlign: "center",
    fontWeight: "bold",
    fontStyle: "italic"
    // marginBottom: getReponsive.fontSize(9)
  },
  errorSubTitle: {
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
    color: colors.text.white.normal,
    textAlign: "center",
    textDecorationLine: "underline"
  },
  blankSpace: {
    height: getReponsive.height(73.5)
  },
  buttonText: {
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.button,
    color: colors.text.orange
  },
  button: {
    backgroundColor: colors.secondary,
    width: getReponsive.width(180),
    height: getReponsive.height(52)
  },
  icon: {
    color: colors.text.white.normal
  }
});

export default styles;
