import React, { Component } from "react";
import { View, Text, Image, TextInput } from "react-native";
import Icon from "react-native-vector-icons/Entypo";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { observer } from "mobx-react";
import Button from "./../../../components/Button";
import DismissKeyboard from "./../../../components/DismissKeyboard";
import HeaderLeft from "./../../../components/HeaderLeft";
import { colors, getReponsive } from "../../../assets/constants";
import styles from "./styles";

@observer
export default class ForgotScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      borderBottomWidth: 0,
      shadowOpacity: 0,
      elevation: 0,
      backgroundColor: colors.primary.orange
    },
    headerLeft: (
      <HeaderLeft
        style={styles.buttonBack}
        goBack={() => navigation.goBack()}
      />
    )
  });

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      confirmEmail: "",
      noEmail: true
    };
  }

  onConfirmEmail = () => {
    if (
      this.state.email === this.state.confirmEmail &&
      this.state.email !== ""
    ) {
      this.setState({ noEmail: true });
      this.props.navigation.navigate("Confirmed");
    } else {
      this.setState({ noEmail: false });
    }
  };

  render() {
    return (
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={{ backgroundColor: colors.background.orange }}
      >
        <DismissKeyboard>
          <View style={styles.container}>
            <Image
              style={styles.logo}
              source={require("./../../../assets/ic_logo.png")}
              resizeMode="stretch"
            />
            <View style={styles.contentContainer}>
              <View style={styles.titleWrapper}>
                <Text style={styles.title}>Retrouvez votre compte</Text>
                <Text style={styles.subTitle}>
                  Veuillez saisir votre adresse
                </Text>
                <Text style={styles.subTitle}>
                  email pour chercher votre compte
                </Text>
              </View>
              <View style={styles.inputWrapper}>
                <Icon name="mail" size={23} style={styles.icon} />
                <TextInput
                  value={this.state.email}
                  placeholder={"Entrez votre email"}
                  placeholderTextColor={colors.text.white.placeholder}
                  onChangeText={text => this.setState({ email: text })}
                  style={styles.textInput}
                  autoCorrect={false}
                  autoCapitalize="none"
                  underlineColorAndroid="transparent"
                  returnKeyType="next"
                  autoCompleteType="email"
                  onSubmitEditing={() => this.confirmEmailRef.focus()}
                />
              </View>
              <View style={[styles.inputWrapper, { marginBottom: 0 }]}>
                <Icon name="mail" size={23} style={styles.icon} />
                <TextInput
                  value={this.state.confirmEmail}
                  placeholder={"Confirmez votre email"}
                  placeholderTextColor={colors.text.white.placeholder}
                  onChangeText={text => this.setState({ confirmEmail: text })}
                  style={styles.textInput}
                  autoCorrect={false}
                  autoCapitalize="none"
                  underlineColorAndroid="transparent"
                  returnKeyType="done"
                  autoCompleteType="email"
                  ref={ref => (this.confirmEmailRef = ref)}
                />
              </View>
              {!this.state.noEmail ? (
                <View style={styles.errorContainer}>
                  <Text style={styles.errorTitle}>
                    Aucun résultat de recherche
                  </Text>
                  <Text style={styles.errorSubTitle}>
                    Veuillez saisir une autre adresse
                  </Text>
                </View>
              ) : (
                <View style={styles.blankSpace}></View>
              )}
              <Button
                style={styles.button}
                onPress={this.onConfirmEmail}
                labelStyle={styles.buttonText}
                title="Valider"
              />
            </View>
          </View>
        </DismissKeyboard>
      </KeyboardAwareScrollView>
    );
  }
}
