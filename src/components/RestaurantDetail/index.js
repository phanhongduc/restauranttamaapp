import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Modal,
  Dimensions,
  Platform,
  Linking,
  Alert
} from "react-native";
import FastImage from "react-native-fast-image";
import Carousel from "react-native-snap-carousel";
import IconM from "react-native-vector-icons/MaterialIcons";
import IconZ from "react-native-vector-icons/Zocial";
import IconFA5 from "react-native-vector-icons/FontAwesome5";
import { observer, inject } from "mobx-react";
import RestaurantCardItem from "./../../components/RestaurantCardItem";
import { colors } from "./../../assets/constants";
import styles from "./styles";

const { width } = Dimensions.get("window");
const isIOS = Platform.OS === "ios";

@inject("restaurantDetailStore", "headerTitleStore")
@observer
export default class RestaurantDetail extends Component {
  constructor(props) {
    super(props);
  }

  renderItem = ({ item, index }) => {
    const { restaurantDetailStore } = this.props;

    return (
      <TouchableOpacity
        onPress={() => restaurantDetailStore.onPressImage(index)}
      >
        <FastImage
          style={styles.imageStyle}
          source={{ uri: item }}
          resizeMode={FastImage.resizeMode.stretch}
        />
      </TouchableOpacity>
    );
  };

  renderSeparator = () => <View style={styles.separator} />;

  renderGalleryModal = () => {
    const { restaurantDetailStore, restaurant } = this.props;

    return (
      <Modal
        visible={restaurantDetailStore.openGalleryModal}
        animationType="fade"
        transparent
        hardwareAccelerated
      >
        <View style={styles.modal}>
          <View style={styles.modalGalleryContainer}>
            <View style={styles.modalWrapper}>
              <Carousel
                sliderWidth={width}
                sliderHeight={width * 2}
                itemWidth={width * 0.9}
                data={restaurant.images.slice()}
                renderItem={this.renderItemGalleryModal}
                inactiveSlideScale={0.94}
                inactiveSlideOpacity={0}
                containerCustomStyle={styles.modalSlider}
                firstItem={restaurantDetailStore.selectedImgIdx}
              />
            </View>
            <TouchableOpacity
              style={
                isIOS
                  ? styles.modalLeftCloseButton
                  : styles.modalRightCloseButton
              }
              onPress={restaurantDetailStore.toggleGalleryModal}
            >
              <IconM
                name="close"
                size={30}
                color={colors.background.gray.dot}
              />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  renderItemGalleryModal = ({ item }) => {
    return (
      <View style={styles.item}>
        <Image
          source={{ uri: item }}
          style={styles.modalImage}
          resizeMode="contain"
        />
      </View>
    );
  };

  handleOnPressMail = mail => {
    if (!mail) return;
    Linking.canOpenURL("mailto:patachouxtahiti@gmail.com")
      .then(supported => {
        if (supported) {
          Linking.openURL("mailto:patachouxtahiti@gmail.com");
        } else {
          Linking.openURL(
            "https://accounts.google.com/ServiceLogin?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1"
          );
        }
      })
      .catch(error => console.log(error));
  };

  handleOnPressFacebook = facebook => {
    if (!facebook) return;
    Linking.canOpenURL("fb://profile/410484879066269")
      .then(supported => {
        if (supported) {
          Linking.openURL("fb://profile/410484879066269");
        } else {
          Linking.openURL(facebook);
        }
      })
      .catch(error => console.log(error));
  };

  handleOnPressInstagram = instagram => {
    if (!instagram) return;
    Linking.canOpenURL("instagram://user?username=apple")
      .then(supported => {
        if (supported) {
          Linking.openURL("instagram://user?username=apple");
        } else {
          Linking.openURL(instagram);
        }
      })
      .catch(error => console.log(error));
  };

  handleOnOpenWebsite = website => {
    if (!website) return;
    Linking.canOpenURL("http://www.patachouxtahiti.com/")
      .then(supported => {
        if (supported) {
          Linking.openURL(website);
        }
      })
      .catch(error => console.log(error));
  };

  handleOnMakeCall = phoneNumber => {
    const actionURL = `tel:${phoneNumber}`;
    Linking.canOpenURL(actionURL)
      .then(supported => {
        if (!supported) {
          Alert.alert("Phone number is not available");
        } else {
          return Linking.openURL(actionURL);
        }
      })
      .catch(err => console.log(err));
  };

  render() {
    const { restaurant } = this.props;

    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={styles.headerWrapper}>
          <RestaurantCardItem
            containerStyle={styles.cardContainer}
            infoContainerStyle={styles.cardInfoContainer}
            nameStyle={styles.nameStyle}
            restaurant={restaurant}
          />
        </View>
        <View style={styles.socialAction}>
          <TouchableOpacity style={styles.iconSocialWrapper}>
            <IconM name="location-on" size={30} color={colors.text.gray.tab} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.iconSocialWrapper}
            onPress={this.handleOnPressMail.bind(this, restaurant.email)}
          >
            <IconZ name="email" size={27} color={colors.text.gray.tab} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.iconSocialWrapper}
            onPress={this.handleOnPressFacebook.bind(this, restaurant.facebook)}
          >
            <IconFA5 name="facebook" size={25} color={colors.text.gray.tab} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.iconSocialWrapper}
            onPress={this.handleOnPressInstagram.bind(
              this,
              restaurant.instagram
            )}
          >
            <IconFA5 name="instagram" size={25} color={colors.text.gray.tab} />
          </TouchableOpacity>
        </View>
        <View style={styles.infoDetailContainer}>
          <View style={styles.infoDetailWrapper}>
            <Text style={styles.infoDetailDescription}>
              Promenade de Nice Papeete
            </Text>
            <Text style={styles.infoDetailOpenTime}>
              Lundi à vendredi: 05h30 - 18h00
            </Text>
            <Text style={styles.infoDetailOpenTime}>
              Samedi: 05h30 - 14h305
            </Text>
            <View style={styles.infoDetailPhoneNumberWrapper}>
              <TouchableOpacity
                onPress={this.handleOnMakeCall.bind(
                  this,
                  restaurant.phoneNumber[0]
                )}
              >
                <Text style={styles.infoDetailPhoneNumber}>
                  {restaurant.phoneNumber[0]}
                </Text>
              </TouchableOpacity>
              <Text style={styles.infoDetailPhoneNumber}> - </Text>
              <TouchableOpacity
                onPress={this.handleOnMakeCall.bind(
                  this,
                  restaurant.phoneNumber[1]
                )}
              >
                <Text style={styles.infoDetailPhoneNumber}>
                  {restaurant.phoneNumber[1]}
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={this.handleOnPressMail}>
              <Text style={styles.infoDetailAddress}>{restaurant.email}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.handleOnOpenWebsite}>
              <Text style={styles.infoDetailAddress}>{restaurant.website}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <FlatList
          columnWrapperStyle={styles.columnStyle}
          keyExtractor={(item, index) => index.toString()}
          data={restaurant.images.slice()}
          renderItem={this.renderItem}
          numColumns={3}
          ItemSeparatorComponent={this.renderSeparator}
          scrollEnabled={false}
        />
        <View style={styles.blankArea} />
        {this.renderGalleryModal()}
      </ScrollView>
    );
  }
}
