import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { inject, observer } from "mobx-react";
import styles from "./styles";

@inject("keyboardStore")
@observer
export default class Keyboard extends Component {
  handleOnPressKeyboard = num => {
    const { onPress, keyboardStore } = this.props;
    keyboardStore.onInput(num);
    if (onPress) {
      onPress();
    }
  };

  render() {
    const { style, color } = this.props;
    const containerStyle = [styles.container, style];
    const textStyle = [styles.numText, !!color?{ color }:{}];

    return (
      <View style={containerStyle}>
        <View style={styles.row}>
          <TouchableOpacity
            style={styles.numWrapper}
            onPress={() => this.handleOnPressKeyboard("1")}
          >
            <Text style={textStyle}>1</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numWrapper}
            onPress={() => this.handleOnPressKeyboard("2")}
          >
            <Text style={textStyle}>2</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numWrapper}
            onPress={() => this.handleOnPressKeyboard("3")}
          >
            <Text style={textStyle}>3</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <TouchableOpacity
            style={styles.numWrapper}
            onPress={() => this.handleOnPressKeyboard("4")}
          >
            <Text style={textStyle}>4</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numWrapper}
            onPress={() => this.handleOnPressKeyboard("5")}
          >
            <Text style={textStyle}>5</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numWrapper}
            onPress={() => this.handleOnPressKeyboard("6")}
          >
            <Text style={textStyle}>6</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <TouchableOpacity
            style={styles.numWrapper}
            onPress={() => this.handleOnPressKeyboard("7")}
          >
            <Text style={textStyle}>7</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numWrapper}
            onPress={() => this.handleOnPressKeyboard("8")}
          >
            <Text style={textStyle}>8</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.numWrapper}
            onPress={() => this.handleOnPressKeyboard("9")}
          >
            <Text style={textStyle}>9</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <View style={styles.numWrapper} />
          <TouchableOpacity
            style={styles.numWrapper}
            onPress={() => this.handleOnPressKeyboard("0")}
          >
            <Text style={textStyle}>0</Text>
          </TouchableOpacity>
          <View style={styles.numWrapper} />
        </View>
      </View>
    );
  }
}
