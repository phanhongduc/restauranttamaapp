import { StyleSheet, Dimensions } from "react-native";
import { fonts, colors, getReponsive } from "./../../assets/constants";

// const { height } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    // height: height * 0.355
    width: "80%",
    aspectRatio: 4 / 3
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    height: "25%",
    justifyContent: "space-between"
  },
  numWrapper: {
    height: "100%",
    width: "20%",
    justifyContent: "center",
    alignItems: "center"
  },
  numText: {
    fontSize: getReponsive.fontSize(45),
    color: colors.primary.orange,
    fontFamily: fonts.primary.regular
  }
});

export default styles;
