import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    // marginBottom: 65,
    alignItems: "center",
    justifyContent: "center"
    // backgroundColor: "yellow"
  },
  container: {
    flex: 1,
    zIndex: 2,
    alignItems: "center"
  },
  dragHandler: {
    alignSelf: "stretch",
    height: 42,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 24,
    position: "relative",
    zIndex: 3,
    backgroundColor: "white"
  },
  icon: {
    position: "absolute",
    top: 25
  }
});

export default styles;
