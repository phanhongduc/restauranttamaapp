import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import SlidingUpPanel from "rn-sliding-up-panel";
import { Header } from "react-navigation";
import { View, TouchableOpacity, Text, ScrollView } from "react-native";
import { StyleSheet, Dimensions, Animated, Platform } from "react-native";
import RestaurantDetail from "../RestaurantDetail";
import Icon from "react-native-vector-icons/Feather";
const { height, width } = Dimensions.get("window");
const isIOS = Platform.OS === "ios";
const top = height - Header.HEIGHT;
const bottomIOS = (height * 31) / 100;
const bottomAndroid = (height * 42) / 100;
const heightContentSlider = top;
import styles from "./styles";

const bottom = isIOS ? bottomIOS : bottomAndroid;

@observer
export default class PreviewDetailRestaurant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShow: true
    };
  }
  _show = () => {
    this._panel.show();
    this.setState({
      isShow: false
    });
  };
  _hide = () => {
    this._panel.hide();
    this.setState({
      isShow: true
    });
  };
  _animatedValue = new Animated.Value(200);
  componentDidMount() {
    this._animatedValue.addListener(this._onAnimatedValueChange);
  }

  componentWillUnmount() {
    this._animatedValue.removeListener(this._onAnimatedValueChange);
  }

  _onAnimatedValueChange = ({ value }) => {
    if (value === top) {
      this.setState({ isShow: false });
    } else if (value === bottom) {
      this.setState({ isShow: true });
    }
  };

  render() {
    const _isShow = this.state.isShow;
    console.log(this.props.navigation);
    const restaurant = this.props.navigation.getParam("restaurant");
    return (
      <View style={styles.wrapper}>
        <SlidingUpPanel
          ref={c => (this._panel = c)}
          draggableRange={{ top: top, bottom: bottom }}
          height={heightContentSlider}
          animatedValue={this._animatedValue}
          snappingPoints={[360]}
        >
          {dragHandlers => (
            <View style={styles.container}>
              <View {...dragHandlers} style={styles.dragHandler}>
                {!_isShow && (
                  <Icon
                    name="chevron-down"
                    size={22}
                    color="#000"
                    onPress={() => this._hide()}
                    style={styles.icon}
                  />
                )}
                {_isShow && (
                  <Icon
                    name="chevron-up"
                    size={22}
                    color="#000"
                    onPress={() => this._show()}
                    style={styles.icon}
                  />
                )}
              </View>

              <RestaurantDetail restaurant={restaurant} />
            </View>
          )}
        </SlidingUpPanel>
      </View>
    );
  }
}
