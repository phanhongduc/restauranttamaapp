import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, Linking } from "react-native";
import { observer, inject } from "mobx-react";
import styles from "./styles";

const callIcon = require("./../../assets/ic_call.png");
const cardIcon = require("./../../assets/ic_card_orange.png");

@inject("restaurantCardItemStore")
@observer
export default class RestaurantCardItem extends Component {
  handleOnMakeCall = phoneNumber => {
    // const { phoneNumber } = this.props;
    // const phoneNumber = ["234223942", "234234234"];
    // switch (phoneNumber.length) {
    //   case 0:
    //     break;
    //   case 1: {
    //     const actionURL = `tel:${phoneNumber}`;
    //     Linking.canOpenURL(actionURL)
    //       .then(supported => {
    //         if (!supported) {
    //           Alert.alert("Phone number is not available");
    //         } else {
    //           return Linking.openURL(actionURL);
    //         }
    //       })
    //       .catch(err => console.log(err));
    //     break;
    //   }
    //   case 2:
    //     break;
    // }
    // const phoneNumber = "84123456789";
    const actionURL = `tel:${phoneNumber}`;
    Linking.canOpenURL(actionURL)
      .then(supported => {
        if (!supported) {
          Alert.alert("Phone number is not available");
        } else {
          return Linking.openURL(actionURL);
        }
      })
      .catch(err => console.log(err));
  };

  render() {
    const {
      containerStyle,
      infoContainerStyle,
      nameStyle,
      restaurant
    } = this.props;

    return (
      <View style={[styles.container, containerStyle]}>
        <Image
          source={{
            uri: restaurant.thumbnail
          }}
          style={styles.imageStyle}
          resizeMode="stretch"
        />
        <View style={[styles.infoContainer, infoContainerStyle]}>
          <View style={styles.contentContainer}>
            <Text style={[styles.name, nameStyle]}>{restaurant.name}</Text>
            <Text style={styles.time}>Ouvert / Ferme à: 14h00</Text>
            <View style={styles.cardRowContainer}>
              <Image
                source={cardIcon}
                style={styles.cardIcon}
                resizeMode="stretch"
              />
              <Text style={styles.cardText}>Carte individuelle acceptée</Text>
            </View>
          </View>
          {/* <View style={styles.callIconContainer}>
            <TouchableOpacity
              style={styles.callIconWrapper}
              onPress={this.handleOnMakeCall.bind(this, restaurant.phoneNumber[0])}
            >
              <Image source={callIcon} style={styles.callIcon} />
            </TouchableOpacity>
          </View> */}
        </View>
      </View>
    );
  }
}
