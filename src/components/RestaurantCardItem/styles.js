import { StyleSheet } from "react-native";
import {
  colors,
  fontSizes,
  fonts,
  getReponsive
} from "./../../assets/constants";

const styles = StyleSheet.create({
  container: {
    width: "82%",
    height: 52,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  imageStyle: {
    height: getReponsive.height(67),
    width: getReponsive.width(64)
  },
  infoContainer: {
    width: "78%",
    height: "100%",
    flexDirection: "row",
    marginLeft: getReponsive.width(16)
  },
  contentContainer: {
    width: "77%",
    height: "100%",
    justifyContent: "space-between"
  },
  name: {
    fontSize: fontSizes.detail.big,
    fontFamily: fonts.primary.semibold,
    color: colors.text.gray.title
  },
  time: {
    fontSize: fontSizes.detail.small,
    color: colors.text.gray.transaction,
    fontFamily: fonts.primary.medium
  },
  cardRowContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  cardIcon: {
    width: getReponsive.width(15),
    height: getReponsive.height(10)
  },
  cardText: {
    fontSize: fontSizes.detail.tiny,
    color: colors.text.gray.tab,
    fontFamily: fonts.primary.regular,
    marginHorizontal: getReponsive.width(8)
  },
  aboutIcon: {
    width: 9,
    height: 9
  },
  callIconContainer: {
    width: "23%",
    height: "100%",
    justifyContent: "center",
    borderLeftWidth: 1,
    borderLeftColor: "#E8E8E8"
  },
  callIconWrapper: {
    flex: 1,
    justifyContent: "center"
  },
  callIcon: {
    marginLeft: "35.65%"
  }
});

export default styles;
