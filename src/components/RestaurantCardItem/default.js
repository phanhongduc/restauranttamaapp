import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Linking,
} from "react-native";
import { observer, inject } from 'mobx-react';
import styles from "./styles";

const callIcon = require("./../../assets/ic_call.png");
const cardIcon = require("./../../assets/ic_card_green.png");

@inject("restaurantCardItemStore")
@observer
export default class RestaurantDefaultCardItem extends Component {
  handleOnMakeCall = () => {
    // const { phoneNumber } = this.props;
    // const phoneNumber = ["234223942", "234234234"];
    // switch (phoneNumber.length) {
    //   case 0:
    //     break;
    //   case 1: {
    //     const actionURL = `tel:${phoneNumber}`;
    //     Linking.canOpenURL(actionURL)
    //       .then(supported => {
    //         if (!supported) {
    //           Alert.alert("Phone number is not available");
    //         } else {
    //           return Linking.openURL(actionURL);
    //         }
    //       })
    //       .catch(err => console.log(err));
    //     break;
    //   }
    //   case 2:
    //     break;
    // }
    const phoneNumber = "84123456789";
    const actionURL = `tel:${phoneNumber}`;
    Linking.canOpenURL(actionURL)
      .then(supported => {
        if (!supported) {
          Alert.alert("Phone number is not available");
        } else {
          return Linking.openURL(actionURL);
        }
      })
      .catch(err => console.log(err));
  };

  render() {
    const { containerStyle, infoContainerStyle, nameStyle } = this.props;

    return (
      <View style={[styles.container, containerStyle]}>
        <Image
          source={{
            uri:
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcREZAOd0c8PfZAoH44mszaUIQfAT1HmhdEPsx4j4KtaNX_cGTEc"
          }}
          style={styles.imageStyle}
        />
        <View style={[styles.infoContainer, infoContainerStyle]}>
          <View style={styles.contentContainer}>
            <Text style={[styles.name, nameStyle]}>Patachoux Tahiti</Text>
            <Text style={styles.time}>Ouvert / Ferme à: 14h00</Text>
            <View style={styles.cardRowContainer}>
              <Image source={cardIcon} style={styles.cardIcon} />
              <Text style={styles.cardText}>Carte individuelle acceptée</Text>
            </View>
          </View>
          <View style={styles.callIconContainer}>
            <TouchableOpacity
              style={styles.callIconWrapper}
              onPress={this.handleOnMakeCall}
            >
              <Image source={callIcon} style={styles.callIcon} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
