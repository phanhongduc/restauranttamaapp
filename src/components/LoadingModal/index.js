import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, Modal } from "react-native";
import { observer } from "mobx-react";
import { fonts, fontSizes, colors } from "./../../assets/constants";

@observer
export default class LoadingModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: "center" }}>
        <Modal
          visible={this.state.isLoading}
          style={{ flex: 1, backgroundColors: colors.background.overlay }}
          animationType={"fade"}
          onRequestClose={() => {}}
        >
          <Text>Loading</Text>
        </Modal>
      </View>
    );
  }
}
