import { StyleSheet } from "react-native";
import { colors, fontSizes, fonts } from "./../../assets/constants";

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 65,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  leftRightIconWrapper: {
    flex: 2,
    alignItems: "center",
    justifyContent: 'center',
  },
  leftRightIcon: {
    width: 23,
    height: 23,
  },
  tabLabel: {
    // flex: 1,
    fontFamily: fonts.primary.regular,
    fontSize: fontSizes.detail.small,
  },
  qrIconContainer: {
    flex: 1,
    alignItems: "center"
  },
  qrIconWrapper: {
    width: 60,
    height: 60,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.primary.orange,
    marginBottom: 60,
    marginRight: 5
  },
  qrIcon: {
    flex: 1
  },
  tabLabelActive: {
    color: colors.text.orange,
  }
});

export default styles;
