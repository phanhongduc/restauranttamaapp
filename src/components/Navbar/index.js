import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity
} from "react-native";
import { observer, inject } from "mobx-react";
import { withNavigation } from "react-navigation";
import styles from "./styles";

@inject("navbarStore")
@observer
class Navbar extends Component {
  constructor(props) {
    super(props);
  }

  goToTab = tab => {
    const { navbarStore, navigation } = this.props;
    navbarStore.setSelectedTab(tab);
    this.props.navigation.navigate(tab);
  };

  render() {
    const { navbarStore } = this.props;

    return (
      <ImageBackground
        source={require("../../assets/it_tab_background.png")}
        style={styles.container}
        resizeMode={"stretch"}
      >
        <TouchableOpacity
          style={styles.leftRightIconWrapper}
          onPress={() => this.goToTab("HomeTab")}
        >
          <Image
            source={
              navbarStore.selected === "HomeTab"
                ? require("../../assets/ic_tab_restaurant_active.png")
                : require("../../assets/ic_tab_restaurant.png")
            }
            style={styles.leftRightIcon}
            resizeMode={"stretch"}
          />
          <Text
            style={[
              styles.tabLabel,
              navbarStore.selected === "HomeTab" ? styles.tabLabelActive : {}
            ]}
          >
            Restaurants
          </Text>
        </TouchableOpacity>
        <View style={styles.qrIconContainer}>
          {navbarStore.selected !== "QRScanTab" && (
            <TouchableOpacity
              style={styles.qrIconWrapper}
              onPress={() => this.goToTab("QRScanTab")}
            >
              <Image
                source={require("../../assets/ic_tab_qrcode.png")}
                style={styles.qrIcon}
                resizeMode={"contain"}
              />
            </TouchableOpacity>
          )}
        </View>
        <TouchableOpacity
          style={styles.leftRightIconWrapper}
          onPress={() => this.goToTab("ProfileTab")}
        >
          <Image
            source={
              navbarStore.selected === "ProfileTab"
                ? require("../../assets/ic_tab_profile_active.png")
                : require("../../assets/ic_tab_profile.png")
            }
            style={styles.leftRightIcon}
            resizeMode={"stretch"}
          />
          <Text
            style={[
              styles.tabLabel,
              navbarStore.selected === "ProfileTab" ? styles.tabLabelActive : {}
            ]}
          >
            Profil
          </Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}

export default withNavigation(Navbar);
