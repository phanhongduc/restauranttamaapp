import React, { Component } from "react";
import { Text } from "react-native";
import { observer, inject } from "mobx-react";
import styles from "./styles";

@inject("headerTitleStore")
@observer
export default class HeaderTitle extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { title, titleStyle, headerTitleStore } = this.props;
    const titleTxt = headerTitleStore.title ? headerTitleStore.title : title;

    return <Text style={[styles.titleStyle, titleStyle]}>{titleTxt}</Text>;
  }
}
