import { StyleSheet, Dimensions } from "react-native";
import { fontSizes, fonts, colors } from "./../../assets/constants";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    width: width * 0.67,
    // aspectRatio: 175 / 52,
    maxHeight: 52,
    backgroundColor: colors.primary.orange,
    borderRadius: 26,
    justifyContent: "center",
    alignItems: "center"
  },
  label: {
    color: colors.text.white.normal,
    fontFamily: fonts.primary.medium,
    fontSize: fontSizes.button,
    textAlign: "center",
    textAlignVertical: "center"
  }
});

export default styles;
