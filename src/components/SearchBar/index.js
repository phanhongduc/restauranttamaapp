import React, { Component } from "react";
import { Text, TouchableOpacity, Keyboard, TextInput, Platform, View } from "react-native";
import { observer, inject } from "mobx-react";
import styles from "./styles";
import Autocomplete from 'react-native-autocomplete-input';
import IconIonic from "react-native-vector-icons/Ionicons";
@inject("headerTitleStore")
@observer
export default class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hideResult: false
        }
    }
    onSearchTextChange = text => {
        const { onChangeText } = this.props;
        this.setState({ hideResult: false })
        onChangeText(text)
    }
    onMoveToMarkerHandle = (lng, lat, id) => {
        const { moveToMarker } = this.props;
        this.setState({ hideResult: true })
        this._keyboardDidHide();
        moveToMarker(lng, lat, id);
    }
    _keyboardDidHide() {
        Keyboard.dismiss();
    }
    renderItem = (item) => {
        const { id, longitude, latitude } = item;
        return (
            <TouchableOpacity style={styles.suggestItem} onPress={() => this.onMoveToMarkerHandle(longitude, latitude, id)}>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }}>
                    <IconIonic name="ios-pin" size={22} style={{ color: '#e6e6e6', marginRight: 5 }} />
                    <Text style={styles.itemText}>
                        {item.restaurantName}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
    renderInput = props => (
        <TextInput {...props} autoCorrect={false} keyboardType={Platform.OS === "android" ? "visible-password" : "default"} ref={component => this._textInput = component} />
    );
    onFocus = () => {
        this.setState({ hideResult: false });
    };
    render() {
        const { value, onSearchTextChange, items } = this.props;
        return (
            <Autocomplete
                renderTextInput={this.renderInput}
                autoCapitalize="none"
                autoCorrect={false}
                containerStyle={styles.autocompleteContainer}
                data={value.length > 1 ? items : []}
                defaultValue={value}
                onChangeText={this.onSearchTextChange}
                placeholder="Trouvez un endroit où manger"
                renderItem={({ item }) => this.renderItem(item)}
                hideResults={this.state.hideResult}
                keyboardShouldPersistTaps='always'
                keyExtractor={(item, index) => index.toString()}
                onFocus={this.onFocus}
                listStyle={{
                    margin: 0,
                    maxHeight: 170,
                }}
                inputContainerStyle={{
                    paddingHorizontal: 10
                }}
            />
        );
    }
}
