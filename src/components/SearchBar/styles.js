import { StyleSheet } from "react-native";
import { colors, fontSizes, fonts } from "./../../assets/constants";

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#F5FCFF',
        flex: 1,
        padding: 16,
        marginTop: 40,
      },
      autocompleteContainer: {
        backgroundColor: '#ffffff',
        borderWidth: 0,
      },
      descriptionContainer: {
        flex: 1,
        justifyContent: 'center',
      },
      itemText: {
        fontSize: 15,
        paddingTop: 10,
        paddingBottom: 10,
        margin: 2,
      },
      infoText: {
        textAlign: 'center',
        fontSize: 16,
      },
      suggestItem: {
        borderTopColor: '#e6e6e6',
        borderTopWidth: 1
      }
});

export default styles;
