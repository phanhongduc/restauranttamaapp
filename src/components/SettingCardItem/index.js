import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { observer } from "mobx-react";
import { colors } from "./../../assets/constants";
import styles from "./styles";

@observer
export default class SettingCardItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation, title, page, renderIcon } = this.props;
    return (
      <TouchableOpacity
        style={styles.cardItemView}
        onPress={() => navigation.navigate(page)}
      >
        <View style={styles.cardItemTitleView}>
          <View style={styles.cardIconWrapper}>
            {renderIcon()}
          </View>
          <Text style={styles.cardItemText}>{title}</Text>
        </View>
        <View style={styles.cardItemIconView}>
          <Icon name="chevron-right" size={35} color={colors.text.gray.chevronRight} />
        </View>
      </TouchableOpacity>
    );
  }
}
