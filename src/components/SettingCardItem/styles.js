import React from "react";
import { StyleSheet, Dimensions, Platform } from "react-native";
import { fontSizes, fonts, colors, getReponsive } from "../../assets/constants";

const screen = Dimensions.get("window");

const styles = StyleSheet.create({
  cardItemView: {
    width: screen.width * 0.8,
    height: 50,
    backgroundColor: colors.background.white,
    borderRadius: 10,
    // margin: 10,
    marginBottom: getReponsive.borderWidth(10),
    padding: getReponsive.borderWidth(10),
    // paddingLeft: 20,
    // paddingRight: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  cardItemTitle: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    flex: 3
  },
  cardItemText: {
    flex: 4,
    fontFamily: fonts.primary.medium,
    color: colors.text.gray.icon,
    fontSize: fontSizes.detail.big
  },
  cardItemIconView: { flex: 1, alignItems: "flex-end" },
  cardItemTitleView: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    flex: 5
  },
  cardIconWrapper: {
    flex: 1
  }
});

export default styles;
