import { StyleSheet, Dimensions } from "react-native";
import { colors, fontSizes, fonts } from "./../../assets/constants";

let {
  width,
  height,
} = Dimensions.get('screen');

// tempory height
// const heightOfBottomNav = 65 * height / 812;
const heightOfBottomNav = 65 ;

const imageSize = {
  width: 228 * width / 375,
  height: 270 * height / 812,
}
const containerHeight = ((height - imageSize.height)/ 2) - heightOfBottomNav;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      // borderWidth: 1,
    },
    image: { 
      marginTop: containerHeight,
      height: imageSize.height, 
      width: imageSize.width,
    },
});

export default styles;
