import React, { Component } from "react";
import { Image, View, } from "react-native";
import { observer, inject } from "mobx-react";
import styles from './styles';
@observer
export default class ScannerMarker extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
   
    render() {
        const { } = this.props;
        return (
            <View style = {styles.container}>
                <Image 
                    style = {styles.image}
                    resizeMode = {"stretch"}
                    source={require('../../assets/scanner_marker.png')}
                />
            </View>
            
        );
    }
}
