import React, { Component } from "react";
import { TouchableWithoutFeedback, Keyboard } from "react-native";
import { observer } from "mobx-react";

@observer
export default class DismissKeyboard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {this.props.children}
      </TouchableWithoutFeedback>
    );
  }
}
