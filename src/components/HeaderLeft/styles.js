import { StyleSheet, Dimensions } from "react-native";
import { colors } from "./../../assets/constants";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    marginLeft: width * 0.0534
  },
  iconContainer: {
    width: 28,
    height: 28,
    justifyContent: "center",
    alignItems: "center"
  },
  icon: {
    color: colors.text.white.normal
  }
});

export default styles;
