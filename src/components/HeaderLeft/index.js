import React, { Component } from "react";
import { View, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { observer } from "mobx-react";
import styles from "./styles";

@observer
export default class HeaderLeft extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { style = {}, goBack } = this.props;

    return (
      <View style={[styles.container, style]}>
        <TouchableOpacity style={styles.iconContainer} onPress={goBack}>
          <Icon name="chevron-left" size={35} style={styles.icon} />
        </TouchableOpacity>
      </View>
    );
  }
}
