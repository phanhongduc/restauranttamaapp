import { observable, action, computed } from "mobx";
import remotedev from "mobx-remotedev";

// @remotedev({ name: "RestaurantDetailStore" })
class RestaurantCardItemStore {
  @observable openChoosePhoneModal = false; // Not use yet

  @action
  toggleChoosePhoneModal = () => {
    this.openChoosePhoneModal = !this.openChoosePhoneModal;
  };
}

const restaurantCardItemStore = new RestaurantCardItemStore();
export default restaurantCardItemStore;
