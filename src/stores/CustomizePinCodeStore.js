import { observable, action, runInAction } from "mobx";
import remotedev from "mobx-remotedev";
import keyboardStore from "./KeyboardStore";

@remotedev({ name: "CustomizePinCodeStore" })
class CustomizePinCodeStore {
  @observable pincodeViewStr = "";
  @observable inputPincodeValue = "";
  @observable confirmPincodeValue = "";

  @observable isInputCompleted = false;
  @observable isDelaying = false; // Delay for animation transition
  @observable disableButton = true;
  @observable inputType = null;
  @observable titleRef = null;
  @observable navigation = null;
  @observable displayError = false;
  @observable errorMessage = "";
  @observable numberFailed = 0;

  MAX_NUMBER_PINCODE = 4;
  INPUT_PINCODE = 1;
  INPUT_CONFIRM_PINCODE = 2;
  REINPUT_PINCODE = 3;
  DELAY_TRANSITION = 100;
  MAXIMUM_FAILURE = 5;

  @action
  initScreen = navigation => {
    this.inputType = this.INPUT_PINCODE;
    this.navigation = navigation;
    keyboardStore.setMaxLength(this.MAX_NUMBER_PINCODE);
    this.numberFailed = 0;
    this.refreshScreen();
  };

  @action
  refreshScreen = () => {
    keyboardStore.clear();
    this.pincodeViewStr = "";
    this.isDelaying = false;
    this.disableButton = true;
    this.isInputCompleted = false;
    this.displayError = false;
    this.errorMessage = "";
  };

  @action
  addPincode = () => {
    if (this.isInputCompleted || this.isDelaying) return;

    this.pincodeViewStr += "* ";
    if (keyboardStore.length !== this.MAX_NUMBER_PINCODE) return;

    switch (this.inputType) {
      case this.INPUT_PINCODE:
      case this.REINPUT_PINCODE:
        this.isDelaying = true;
        setTimeout(() => {
          this.handleCompleteInputPincode();
        }, 100);
        break;
      case this.INPUT_CONFIRM_PINCODE:
        this.handleCompleteInputConfirmPincode();
        break;
    }
  };

  @action
  handleCompleteInputPincode = () => {
    this.inputPincodeValue = keyboardStore.getValue;
    this.titleRef["fadeOut"](this.DELAY_TRANSITION);
    setTimeout(() => {
      runInAction(() => {
        this.inputType = this.INPUT_CONFIRM_PINCODE;
      });
    }, this.DELAY_TRANSITION);
    this.refreshScreen();
  };

  @action
  handleCompleteInputConfirmPincode = () => {
    this.confirmPincodeValue = keyboardStore.getValue;
    this.isInputCompleted = true;
    this.disableButton = false;
    this.handleOnValidate();
  };

  @action
  handleReInputPincode = () => {
    this.isDelaying = true;
    this.titleRef["fadeOut"](this.DELAY_TRANSITION);
    setTimeout(() => {
      this.inputType = this.REINPUT_PINCODE;
    }, this.DELAY_TRANSITION);
    this.refreshScreen();
    this.numberFailed = 0;
  };

  @action
  removePincode = () => {
    if (!this.pincodeViewStr.length) return;
    this.pincodeViewStr = this.pincodeViewStr.substr(
      0,
      this.pincodeViewStr.length - 2
    );
    if (this.isInputCompleted) {
      this.isInputCompleted = false;
    }
    if (!this.disabledButton) {
      this.disableButton = true;
    }
  };

  @action
  handleOnValidate = () => {
    if (this.inputPincodeValue === this.confirmPincodeValue) {
      // If success
      setTimeout(() => {
        this.navigation.navigate("HomeTab");
        this.refreshScreen();
      }, 100);
    } else {
      this.numberFailed += 1;
      if (this.numberFailed === this.MAXIMUM_FAILURE) {
        this.showErrorMessage(
          "You have been blocked.\nPlease try again in 15 seconds",
          15000,
          () => {
            this.inputType = this.REINPUT_PINCODE;
            this.numberFailed = 0;
          }
        );
      } else {
        this.showErrorMessage("Pincode not match.\n Please try again", 2000);
      }
    }
  };

  @action
  showErrorMessage = (message, timeout, callback) => {
    this.refreshScreen();
    this.isDelaying = true;
    keyboardStore.blockKeyboard();
    this.errorMessage = message;
    this.displayError = true;
    setTimeout(() => {
      this.isDelaying = false;
      keyboardStore.unblockKeyboard();
      this.displayError = false;
      if (callback) {
        callback();
      }
    }, timeout);
  };

  @action
  setTitleRef = ref => {
    if (!ref) return;
    this.titleRef = ref;
  };
}

const customizePinCodeStore = new CustomizePinCodeStore();
export default customizePinCodeStore;
