import axios from "axios";
import { observable, computed, action, runInAction } from "mobx";
import remotedev from "mobx-remotedev";

// will change url
const API_URL = "http://5ce659c00adb8e0014a6ef8d.mockapi.io/api/";

// @remotedev({ name: "etape1Store" })
class Etape1Store {
  @observable information = [];
  @observable activeSlide = 0;
  @observable carouselRef = null;
  @observable navigation = null;

  @action
  initScreen = navigation => {
    this.navigation = navigation;
    this.activeSlide = 0;
  };

  // @action
  // getContent = () => {
  //   const requestUrl = API_URL + "profiles";
  //   axios
  //     .get(requestUrl)
  //     .then(response => {
  //       runInAction(() => {
  //         this.information = response.data;
  //         // will change when having api
  //       });
  //       // console.log(this.information)
  //     })
  //     .catch(function(error) {
  //       console.log(error);
  //     });
  // };

  @action
  setActiveSlide = index => {
    this.activeSlide = index;
  };

  @action
  setCarouselRef = ref => {
    this.carouselRef = ref;
  };

  handleHardwareBack = () => {
    if (this.activeSlide) {
      this.activeSlide -= 1;
      this.carouselRef.snapToPrev();
    }
  };
}
const etape1Store = new Etape1Store();
export default etape1Store;
