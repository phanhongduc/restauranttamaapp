import axios from "axios";
import { observable, computed, action, runInAction } from "mobx";
import remotedev from "mobx-remotedev";

// change url later
const API_URL = "http://5ce659c00adb8e0014a6ef8d.mockapi.io/api/";

@remotedev({ name: "mesInfosStore" })
class MesInfosStore {
  @observable infos = "";
  @observable isLoading = false;

  @action
  getInfos = () => {
    const requestUrl = API_URL + "Restaurants";
    this.isLoading = true;

    axios
      .get(requestUrl)
      .then(response => {
        runInAction(() => {
          this.infos = response.data[0];

          // will change when having api
          console.log(response);
        });
        console.log(this.infos);
      })
      .catch(function(error) {
        console.log(error);
        this.isLoading = false;
      });
  };
  @computed get dataContent() {
    let colectData = this.content.map(element => {
      console.log(element);
      return element.data;
    });
    return colectData;
  }
}
const mesInfosStore = new MesInfosStore();
export default mesInfosStore;
