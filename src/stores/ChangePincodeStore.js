import { observable, action, computed, runInAction } from "mobx";
import keyboardStore from "./KeyboardStore";
import remotedev from "mobx-remotedev";

// @remotedev({ name: "ChangePincodeStore" })
class ChangePincodeStore {
  @observable pincodeViewStr = "";
  @observable currPincodeValue = "";
  @observable newPincodeValue = "";
  @observable confirmPincodeValue = "";

  @observable isInputCompleted = false;
  @observable isDelaying = false;
  @observable disableButton = true;
  @observable inputType = null;
  @observable titleRef = null;
  @observable navigation = null;
  @observable displayError = false;
  @observable errorMessage = "";
  @observable numberFailed = 0;

  MAX_NUMBER_PINCODE = 4;
  INPUT_CURRENT_PINCODE = 1;
  INPUT_NEW_PINCODE = 2;
  INPUT_CONFIRM_PINCODE = 3;
  REINPUT_CURRENT_PINCODE = 4;
  REINPUT_NEW_PINCODE = 5;
  DELAY_TRANSITION = 100;
  MAXIMUM_FAILURE = 5;

  @action
  initScreen = navigation => {
    this.inputType = this.INPUT_CURRENT_PINCODE;
    keyboardStore.setMaxLength(this.MAX_NUMBER_PINCODE);
    this.refreshScreen();
    this.navigation = navigation;
  };

  @action
  refreshScreen = () => {
    keyboardStore.clear();
    this.pincodeViewStr = "";
    this.isDelaying = false;
    this.disableButton = true;
    this.isInputCompleted = false;
    this.displayError = false;
    this.errorMessage = "";
  };

  @action
  addPincode = () => {
    if (this.isInputCompleted || this.isDelaying) return;

    this.pincodeViewStr += "* ";
    if (keyboardStore.length !== this.MAX_NUMBER_PINCODE) return;

    switch (this.inputType) {
      case this.INPUT_CURRENT_PINCODE:
      case this.REINPUT_CURRENT_PINCODE:
        this.handleCompleteInputCurrentPincode();
        break;
      case this.INPUT_NEW_PINCODE:
      case this.REINPUT_NEW_PINCODE:
        this.handleCompleteInputNewPincode();
        break;
      case this.INPUT_CONFIRM_PINCODE:
        this.handleCompleteInputConfirmPincode();
        break;
    }
  };

  @action
  handleCompleteInputCurrentPincode = () => {
    this.currPincodeValue = keyboardStore.getValue;
    this.titleRef["fadeOut"](this.DELAY_TRANSITION);
    this.isDelaying = true;
    setTimeout(() => {
      runInAction(() => {
        this.refreshScreen();
        this.inputType = this.INPUT_NEW_PINCODE;
      });
    }, this.DELAY_TRANSITION);
  };

  @action
  handleCompleteInputNewPincode = () => {
    this.newPincodeValue = keyboardStore.getValue;
    if (this.isPincodeValid) {
      this.titleRef["fadeOut"](this.DELAY_TRANSITION);
      this.isDelaying = true;
      setTimeout(() => {
        runInAction(() => {
          this.refreshScreen();
          this.inputType = this.INPUT_CONFIRM_PINCODE;
        });
      }, this.DELAY_TRANSITION);
    } else {
      this.showErrorMessage(
        "Your new pin code cannot be same as the old pin code",
        2000
      );
    }
  };

  @action
  handleCompleteInputConfirmPincode = () => {
    this.confirmPincodeValue = keyboardStore.getValue;
    this.isInputCompleted = true;
    this.disableButton = false;
    this.handleOnValidate();
  };

  @action
  handleReInputCurrentPincode = () => {
    this.isDelaying = true;
    this.titleRef["fadeOut"](this.DELAY_TRANSITION);
    setTimeout(() => {
      this.inputType = this.REINPUT_CURRENT_PINCODE;
    }, this.DELAY_TRANSITION);
    this.refreshScreen();
  };

  @action
  handleReInputNewPincode = () => {
    this.isDelaying = true;
    this.titleRef["fadeOut"](this.DELAY_TRANSITION);
    setTimeout(() => {
      this.inputType = this.REINPUT_NEW_PINCODE;
    }, this.DELAY_TRANSITION);
    this.refreshScreen();
    this.numberFailed = 0;
  };

  @action
  removePincode = () => {
    if (!this.pincodeViewStr.length) return;
    this.pincodeViewStr = this.pincodeViewStr.substr(
      0,
      this.pincodeViewStr.length - 2
    );
    if (this.isInputCompleted) {
      this.isInputCompleted = false;
    }
    if (!this.disabledButton) {
      this.disableButton = true;
    }
  };

  @action
  handleOnValidate = () => {
    if (this.newPincodeValue === this.confirmPincodeValue) {
      setTimeout(() => {
        this.navigation.pop();
        this.refreshScreen();
      }, 100);
    } else {
      this.numberFailed += 1;
    if (this.numberFailed === this.MAXIMUM_FAILURE) {
      this.showErrorMessage(
        "You have been blocked. Please try again in 15 seconds",
        15000,
        () => {
          this.inputType = this.REINPUT_NEW_PINCODE;
          this.numberFailed = 0;
        }
        );
      } else {
        this.showErrorMessage("Pincode not match. Please try again", 2000);
      }
    }
  };

  @action
  setTitleRef = ref => {
    if (!ref) return;
    this.titleRef = ref;
  };

  @action
  showErrorMessage = (message, timeout, callback) => {
    this.refreshScreen();
    this.isDelaying = true;
    keyboardStore.blockKeyboard();
    this.errorMessage = message;
    this.displayError = true;
    setTimeout(() => {
      this.isDelaying = false;
      keyboardStore.unblockKeyboard();
      this.displayError = false;
      if (callback) {
        callback();
      }
    }, timeout);
  };

  @action
  handleBlockFailureInput = () => {
    this.errorMessage = "You have been blocked. Please try again in 15 seconds";
    this.displayError = true; 
    setTimeout(() => {
      this.isDelaying = false;
      keyboardStore.unblockKeyboard();
      this.inputType = this.REINPUT_PINCODE;
      this.displayError = false;
      this.numberFailed = 0;
    }, 15000);
  };
  
  @computed
  get isPincodeValid() {
    return this.currPincodeValue !== this.newPincodeValue;
  }
}

const changePincodeStore = new ChangePincodeStore();
export default changePincodeStore;
