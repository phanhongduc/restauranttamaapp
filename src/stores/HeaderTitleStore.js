import { observable, action, computed } from "mobx";
import remotedev from "mobx-remotedev";

class HeaderTitleStore {
  @observable title = null;

  @action
  setTitle = title => {
    this.title = title;
  };
}

const headerTitleStore = new HeaderTitleStore();
export default headerTitleStore;
