import axios from "axios";
import { observable, computed, action, runInAction } from "mobx";
import remotedev from "mobx-remotedev";
import { SERVER_URL } from "./../ultils/API";
import { ToastAndroid } from "react-native";

// @remotedev({ name: "termConditionStore" })
class TermConditionStore {
  @observable content = "";
  @observable isLoading = true;

  // @action
  // getContent = async () => {
  //   try {
  //     const apiURL = SERVER_URL + "/api/information_centres/3";
  //     ToastAndroid.show(`đã chạy đến đây ${apiURL}`,60000);
  //     const response = await axios.get(apiURL, {
  //       headers: {
  //         Accept: 'application/json'
  //       }
  //     });
  //     // ToastAndroid.show(`res: ` + JSON.stringify(response.data),60000);

  //     runInAction(() => {
  //       this.content = response.data; 
  //       this.isLoading = false;
  //     });
  //     } catch (error) {
  //     // ToastAndroid.show(`error: ` + JSON.stringify(error),60000);
  //     console.log('error: ', error);
  //   }
  // };

  // sudo getContent
  @action
  getContent = async () => {
    this.isLoading = false;
    this.content= {
      content: "### We can back it to Terms and "
      + "Conditions\nLast updated: June 20, 2019\n"
      +"\nPlease read these Terms and Conditions "
      +"(\"Terms\", \"Terms and Conditions\") "
      +"carefully before using the website "
      + "(the \"Service\") operated by (\"us\","
      +" \"we\", or \"our\").\n\nYour access to "
      +"and use of the Service is conditioned on "
      +"your acceptance of and compliance with "
      +"these Terms. These Terms apply to all visitors,"
      +" users and others who access or use the Service."
      +"\n\nBy accessing or using the Service you agree "
      +"to be bound by these Terms. If you disagree with"
      +" any part of the terms then you may not access "
      +"the Service. The Terms and Conditions agreement"
      +" has been created with the help of TermsFeed.\n"
      +"\n#### Links To Other Web Sites\nOur Service may "
      +"contain links to third-party web sites or services "
      +"that are not owned or controlled by .\n\nhas no "
      +"control over, and assumes no responsibility for, "
      +"the content, privacy policies, or practices of any"
      +" third party web sites or services. You further "
      +"acknowledge and agree that shall not be responsible "
      + "or liable, directly or indirectly, for any damage or"
      +" loss caused or alleged to be caused by or in connection"
      +" with use of or reliance on any such content, goods or"
      +" services available on or through any such web sites "
      +"or services.\n\nWe strongly advise you to read the "
      +"terms and conditions and privacy policies of any third-party"
      +" web sites or services that you visit.\n\n#### Termination\nWe "
      +"may terminate or suspend access to our Service immediately, "
      +"without prior notice or liability, for any reason whatsoever,"
      +" including without limitation if you breach the Terms.\n\nAll"
      +" provisions of the Terms which by their nature should survive "
      +"termination shall survive termination, including, without limitation, "
      +"ownership provisions, warranty disclaimers, indemnity and limitations of "
      +"liability.\n\n#### Governing Law\nThese Terms shall be governed and construed"
      +" in accordance with the laws of Andhra Pradesh, India, without regard to its"
      +" conflict of law provisions.\n\nOur failure to enforce any right or provision"
      +" of these Terms will not be considered a waiver of those rights. If any provision"
      +" of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.\n\n#### Changes\nWe reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 15 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.\n\nBy continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.\n\n#### Contact Us\nIf you have any questions about these Terms, please contact us.",
    }
  }

  @computed get dataContent() {
    let colectData = this.content.map(element => {
      console.log(element);
      return element.data;
    });
    return colectData;
  }
}
const termConditionStore = new TermConditionStore();
export default termConditionStore;
