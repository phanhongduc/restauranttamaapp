import { observable, action, computed } from "mobx";
import remotedev from "mobx-remotedev";

// @remotedev({ name: "RestaurantDetailStore" })
class RestaurantDetailStore {
  @observable openGalleryModal = false;
  @observable selectedImgIdx = null;

  @action
  toggleGalleryModal = () => {
    this.openGalleryModal = !this.openGalleryModal;
  }

  @action
  onPressImage = (index) => {
    this.selectedImgIdx = index;
    this.toggleGalleryModal();
  }

}

const restaurantDetailStore = new RestaurantDetailStore();
export default restaurantDetailStore;
