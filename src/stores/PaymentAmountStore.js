import { observable, action } from "mobx";
import keyboardStore from "./KeyboardStore";
import remotedev from "mobx-remotedev";

// @remotedev({ name: "PaymentStore" })
class PaymentAmountStore {
  @observable amountViewStr = "";
  @observable balance = "0"; // maximum display +2000
  @observable disableButton = true;
  @observable navigation = null;

  MAX_LENGTH_DIGIT = 6;

  @action
  initScreen = navigation => {
    keyboardStore.clear();
    keyboardStore.setMaxLength(6);
    this.navigation = navigation;
    this.amountViewStr = "";
    this.disableButton = true;
  };

  @action
  addDigit = () => {
    if (this.disableButton) {
      this.disableButton = false;
    }
    this.amountViewStr = keyboardStore.getValue.replace(
      /(\d)(?=(\d{3})+(?!\d))/g,
      "$1 "
    );
  };

  @action
  removeDigit = () => {
    if (!keyboardStore.getValue.length) {
      this.disableButton = true;
    }
    this.amountViewStr = keyboardStore.getValue.replace(
      /(\d)(?=(\d{3})+(?!\d))/g,
      "$1 "
    );
  };

  handleOnValidate = () => {
    this.navigation.navigate("PaymentPincode");
  };
}

const paymentAmountStore = new PaymentAmountStore();
export default paymentAmountStore;
