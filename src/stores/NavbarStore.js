import { observable, action } from "mobx";

class NavbarStore {
  @observable selected = "HomeTab";

  @action
  setSelectedTab = tab => {
    this.selected = tab;
  };
}

const navbarStore = new NavbarStore();
export default navbarStore;
