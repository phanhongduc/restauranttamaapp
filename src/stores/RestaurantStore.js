import axios from "axios";
import { observable, computed, action, runInAction } from "mobx";
import remotedev from "mobx-remotedev";
import { SERVER_URL } from "./../ultils/API";
import ImagePicker from "react-native-image-crop-picker";

const createFormData = (photos = []) => {
  const data = new FormData();

  for (const photo of photos) {
    const { Platform } = require("react-native");

    const part = photo.path.split("/");
    const name = `${part[part.length - 1].replace(" ", "")}.jpg`;
    const uri =
      Platform.OS === "android"
        ? photo.path
        : photo.path.replace("file://", "");

    data.append("photo", {
      name: name,
      type: `image/jpeg`,
      // type: photo.type,
      uri: uri
    });
  }
  return data;
};

class RestaurantStore {
  @observable listRestaurants = null;
  @observable isLoading = true;
  @observable data = [];

  @action
  fetchListRestaurants = async callback => {
    try {
      const apiURL = SERVER_URL + "/api/restaurants";
      const response = await axios.get(apiURL, {
        headers: {
          Accept: "application/json"
        }
      });
      runInAction(() => {
        this.listRestaurants = response.data;
        this.processData();
        this.isLoading = false;
        if (callback) {
          callback();
        }
      });
    } catch (error) {
      console.log("error: ", error);
    }
  };

  @action
  fetchListRestaurants = async callback => {
    this.listRestaurants = [
      {
        id: 5,
        restaurantName: "Snack Vaimiti",
        corporateName: "Vaimiti",
        email: "snackvaimiti@gmail.com",
        description: "Wheelchair accessible entrance, Good for kids, Casual",
        latitude: -17.543489999999998,
        longitude: -149.576097,
        phoneNumber1: "+689 40 53 26 81",
        phoneNumber2: null,
        managerFullName: "Vaimiti",
        managerPhoneNumber: "+689 40 53 26 81",
        address: "Faaa, Plazza Faaa",
        townId: 0,
        joinDate: "2019-06-21T01:55:43+02:00",
        companyRegistration: "Company",
        bankingInfo: "Banking",
        saleComissionPolicy: "Policy",
        images: [
          "https://lh5.googleusercontent.com/p/AF1QipMw9DMMUR7g5NXDHBjU2w5HsIGSPT3ot2ltKVv7=s597-k-no",
          "https://lh5.googleusercontent.com/p/AF1QipOxlon7F8dx4mbob5YcZw5XiwgOYNrfXXy0RsYl=s508-k-no",
          "https://lh5.googleusercontent.com/p/AF1QipNCMxlIQfiYyayuGwqb1yOHpySq83YnEEWTnfwt=s406-k-no",
          "https://lh5.googleusercontent.com/p/AF1QipPbcUqkDOdqFMM0IezHaRIAi1xqjY73-eohwNGf=s435-k-no",
          "https://lh5.googleusercontent.com/p/AF1QipPjGwvf-6y63nhufkDZmEfa8UeMdM4roZzrwKI6=s516-k-no",
          "https://lh5.googleusercontent.com/p/AF1QipPlvIaD5kXulTmHXHA0_M9HnpZz5Kj-ItWIjduF=s508-k-no",
          "https://lh5.googleusercontent.com/p/AF1QipNoHtMSydYa5Kz1Lm674Mym_0Nkw3tlGWB7Hk7H=s516-k-no",
          "https://lh5.googleusercontent.com/p/AF1QipMsuHuvY-ez-BtPyXy2l4uhLtcd2z4xClvyBJjM=s516-k-no",
          "https://lh5.googleusercontent.com/p/AF1QipO5a29MA8UVGAYwv0Ghr-rThldQ9cJRrZSdTijY=s516-k-no",
          "https://lh5.googleusercontent.com/p/AF1QipNTGUEYuyRRv0Wqb71FUsW2owUJ87MrqDYiykUs=s387-k-no"
        ],
        openingHours: [
          "Lundi : 11h00-19h00",
          "Mardi au Samedi : 7h00-19h00",
          "Dimanche : 7h00-12h00",
          "Fériés : 7h00 – 15h00"
        ],
        facebook:
          "https://www.facebook.com/pages/category/Restaurant/Vaimiti-SUSHI-1684216841889050/",
        instagram: null,
        website:
          "https://www.facebook.com/pages/category/Restaurant/Vaimiti-SUSHI-1684216841889050/",
        restaurantId: 10
      }
    ];
    this.processData();
    this.isLoading = false;
  };

  @action
  pickPhotos = async id => {
    try {
      const apiURL = `http://192.168.1.20:3000/`;
      // const apiURL = SERVER_URL + `/api/restaurants/${id}/photos`;
      const options = {
        multiple: true,
        mediaType: `photo`
      };
      const photosFromLibrary = await ImagePicker.openPicker(options);
      const photosForUpload = createFormData(photosFromLibrary);
      let response = await axios.post(apiURL, photosForUpload);
    } catch (error) {
      console.log("error: ", JSON.stringify(error.message));
    }
  };

  @action
  processData = () => {
    const section = ["Pape'ete", "Papeno'o", "Hitimahana"]; // Temporary use
    section.forEach(section => {
      this.data.push({
        section,
        items: this.listRestaurants
      });
    });
  };
}

const restaurantStore = new RestaurantStore();
export default restaurantStore;
