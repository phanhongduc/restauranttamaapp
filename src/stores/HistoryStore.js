import axios from "axios";
import { observable, computed, action, runInAction } from "mobx";
import remotedev from "mobx-remotedev";

// change url later
const API_URL = "http://5ce659c00adb8e0014a6ef8d.mockapi.io/api/";
const API_URLCurrentDay = "http://5ce659c00adb8e0014a6ef8d.mockapi.io/api/";

@remotedev({ name: "historyScreenStore" })
class HistoryScreenStore {
  @observable content;
  @observable information;
  @observable isLoading = false;

  @action
  getInformation = () => {
    const requestUrl = API_URLCurrentDay + "days";

    axios
      .get(requestUrl)
      .then(response => {
        runInAction(() => {
          this.information = response.data;
        });
      })
      .catch(function(error) {
        console.log(error);
        this.isLoading = false;
      });
  };
  
  @computed
  get dataTransactionLastMonth() {
    const d = new Date();
    let m = d.getMonth();

    m = parseInt(m) + 1;
    if (m < 10) {
      m = "0" + m;
    }
    let lm = +m - 1;
    let l2m = +m - 2;
    if (lm < 10) {
      lm = "0" + lm;
    }
    if (l2m < 10) {
      l2m = "0" + l2m;
    }
    y = d.getFullYear();

    let lastTwoMonthsTransaction = [];
    const lastMonth = y + "-" + lm;
    const last2Months = y + "-" + l2m;
    console.log(last2Months);
    let lastOneMonthTransaction = [];

    if (this.information) {
      let dataBy = this.information.map(el => {
        const selectedMonth = el.date.slice(0, 7);
        if (selectedMonth.search(lastMonth) > -1) {
          lastOneMonthTransaction.push(el);
        }
      });
    }
    return lastOneMonthTransaction;
  }

  @computed
  get dataTransactionCurrentMonth() {
    const d = new Date();
    let m = d.getMonth();

    m = parseInt(m) + 1;
    if (m < 10) {
      m = "0" + m;
    }
    y = d.getFullYear();
    let currentMonthTransaction = [];
    let lastOneMonthTransaction = null;
    let lastTwoMonthsTransaction = null;
    console.log(y + "-" + (+m - 2), y + "-" + (+m - 1));
    const lastMonth = y + "-" + (+m - 1);
    const last2Months = y + "-" + (+m - 2);

    if (this.information) {
      let dataBy = this.information.map(el => {
        const selectedMonth = el.date.slice(0, 7);
        if (selectedMonth.search(y + "-" + m) > -1) {
          currentMonthTransaction.push(el);
        } else if (selectedMonth.search(y + "-" + last2Months) > -1) {
          lastTwoMonthsTransaction = [];
          lastTwoMonthsTransaction.push(el);
        }
      });
    }
    console.log(currentMonthTransaction);
    return currentMonthTransaction;
  }

  @computed
  get dataTransactionLast2Months() {
    const d = new Date();
    let m = d.getMonth();

    m = parseInt(m) + 1;
    if (m < 10) {
      m = "0" + m;
    }
    let lm = +m - 1;
    let l2m = +m - 2;
    if (lm < 10) {
      lm = "0" + lm;
    }
    if (l2m < 10) {
      l2m = "0" + l2m;
    }
    y = d.getFullYear();

    let lastTwoMonthsTransaction = [];
    const lastMonth = y + "-" + lm;
    const last2Months = y + "-" + l2m;
    console.log(last2Months);

    if (this.information) {
      let dataBy = this.information.map(el => {
        const selectedMonth = el.date.slice(0, 7);
        if (selectedMonth.search(last2Months) > -1) {
          lastTwoMonthsTransaction.push(el);
        }
      });
    }
    console.log(lastTwoMonthsTransaction);
    return lastTwoMonthsTransaction;
  }
}
const historyScreenStore = new HistoryScreenStore();
export default historyScreenStore;
