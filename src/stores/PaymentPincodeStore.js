import { observable, action, runInAction } from "mobx";
import keyboardStore from "./KeyboardStore";
import remotedev from "mobx-remotedev";

// @remotedev({ name: "PaymentPincodeStore" })
class PaymentPincodeStore {
  @observable pincodeViewStr = "";
  @observable pincodeValue = null;
  @observable balance = "0"; // maximum display +2000
  @observable disableButton = true;
  @observable openBalanceModal = false;
  @observable openCancelModal = false;
  MAX_NUMBER_PINCODE = 4;

  @action
  initScreen = () => {
    keyboardStore.clear();
    keyboardStore.setMaxLength(this.MAX_NUMBER_PINCODE);
    this.pincodeViewStr = "";
    this.pincodeValue = null;
    this.disableButton = true;
  };

  @action
  addDigit = () => {
    if (this.pincodeViewStr.length / 2 === this.MAX_NUMBER_PINCODE) return;

    this.pincodeViewStr += "* ";
    if (this.pincodeViewStr.length / 2 === this.MAX_NUMBER_PINCODE) {
      this.disableButton = false;
    }
  };

  @action
  removeDigit = () => {
    if (!this.pincodeViewStr.length) return;

    this.pincodeViewStr = this.pincodeViewStr.substr(
      0,
      this.pincodeViewStr.length - 2
    );
    if (!this.disabledButton) {
      this.disableButton = true;
    }
  };

  @action
  toggleBalanceModal = () => {
    this.openBalanceModal = !this.openBalanceModal;
  };

  @action
  toggleCancelModal = () => {
    this.openCancelModal = !this.openCancelModal;
  };
}

const paymentPincodeStore = new PaymentPincodeStore();
export default paymentPincodeStore;
