import { observable, action, computed } from "mobx";
import remotedev from "mobx-remotedev";

// @remotedev({ name: "KeyboardStore" })
class KeyboardStore {
  @observable length = 0;
  @observable value = "";
  @observable isBlocking = false;
  @observable MAXLENGTH = 4; // default value

  @action
  onInput = value => {
    if (this.isBlocking || (this.MAXLENGTH && this.length === this.MAXLENGTH))
      return;
    this.length += 1;
    this.value += value;
  };

  @action
  onDelete = callback => {
    if (!this.length) return;
    this.value = this.value.substr(0, this.length - 1);
    this.length -= 1;
    if (callback) {
      callback();
    }
  };

  @action
  clear = () => {
    this.length = 0;
    this.value = "";
  };

  @action
  setMaxLength = maxLength => {
    this.MAXLENGTH = maxLength;
  };

  @computed
  get getValue() {
    return this.value;
  }

  @action
  blockKeyboard = () => {
    this.isBlocking = true;
  };

  @action
  unblockKeyboard = () => {
    this.isBlocking = false;
  };
}

const keyboardStore = new KeyboardStore();
export default keyboardStore;
