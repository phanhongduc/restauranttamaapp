// Radius calculation
// {
/* <View
  style={{
    width: 200,
    borderRadius: 200,
    height: 200,
    borderWidth: 1
  }}
>
  <View style={{ width: 20, height: 20, borderWidth: 1 }}></View>
  <View style={{ width: 20, height: 20, borderWidth: 1 }}></View>
  <View style={{ width: 20, height: 20, borderWidth: 1 }}></View>
  <View style={{ width: 20, height: 20, borderWidth: 1 }}></View>
  <View style={{ width: 20, height: 20, borderWidth: 1 }}></View>
  <View style={{ width: 20, height: 20, borderWidth: 1 }}></View>
  <View style={{ width: 20, height: 20, borderWidth: 1 }}></View>
  <View style={{ width: 20, height: 20, borderWidth: 1 }}></View>
  <View style={{ width: 20, height: 20, borderWidth: 1 }}></View>
  <View style={{ width: 20, height: 20, borderWidth: 1 }}></View>
</View> */
// }

//import fonts, colors, size to all app
const grid = {
  width: 375,
  height: 812
};

import { Dimensions, PixelRatio } from "react-native";

const dpDevice = Dimensions.get("window");

console.log(`dimension`, JSON.stringify(dpDevice));
console.log(`pixel ratio`, JSON.stringify(PixelRatio.get()));
console.log(`pixel ratio`, JSON.stringify(PixelRatio.getFontScale()));
console.log(
  `pixel getPixelSizeForLayoutSize`,
  JSON.stringify(PixelRatio.getPixelSizeForLayoutSize(dpDevice.width))
);
console.log(
  `pixel roundToNearestPixel`,
  JSON.stringify(PixelRatio.roundToNearestPixel(dpDevice.width))
);
console.log(
  `pixel getPixelSizeForLayoutSize.height`,
  JSON.stringify(PixelRatio.getPixelSizeForLayoutSize(dpDevice.height))
);
console.log(
  `pixel roundToNearestPixel`,
  JSON.stringify(PixelRatio.roundToNearestPixel(dpDevice.height))
);

const gridUnit = {
  width: dpDevice.width / grid.width,
  height: dpDevice.height / grid.height
};

console.log(`gridUnit`, JSON.stringify(gridUnit));

export const getReponsive = {
  width: designedSize => {
    return designedSize * gridUnit.width;
  },
  height: designedSize => {
    return designedSize * gridUnit.height;
  },
  borderRadius: designedSize => {
    return designedSize * gridUnit.height;
  },
  borderWidth: designedSize => {
    return designedSize * gridUnit.height;
  },
  fontSize: designedSize => {
    return designedSize * gridUnit.height * 1.1;
  }
};

export const fonts = {
  primary: {
    regular: "Montserrat-Regular",
    bold: "Montserrat-Bold",
    semibold: "Montserrat-SemiBold",
    medium: "Montserrat-Medium",
    light: "Montserrat-Light"
  },
  secondary: {
    medium: "SFProDisplay-Medium",
    regular: "SFProDisplay-Regular"
  }
};

export const colors = {
  primary: {
    green: "#00C68E",
    orange: "#FFB75A"
  },
  secondary: "#FFF",
  background: {
    gray: {
      dot: "#D8D8D8",
      item: "#EEEFEE",
      bar: "#f6f6f6"
    },
    white: "#FFF",
    green: "#00C68E",
    overlay: "rgba(45, 45, 45, 0.5)",
    orange: "#FFB75A",
    nearwhite: "#FBFCFC",
    overlayDark: "rgba(0,0,0,0.95)"
  },
  text: {
    black: "#000",
    white: {
      normal: "#FFF",
      placeholder: "#ffffffCC"
    },
    grayWhite: "#E3E3E3",
    green: "#00C68E",
    orange: "#FFB75A",
    darkOrange: "#e68f1e",
    darkGreen: "#187B5E",
    blackGreen: "#04241A",
    blue: "#007AFF",
    gray: {
      tab: "#747373",
      transaction: "#929191",
      instruction: "#322F2F",
      title: "#393939",
      detail: "#686868",
      border: "#4D4D4D",
      notice: "#00C68E",
      line: "#D8D8D8",
      buttonRefulser: "#B7B5B5",
      icon: "#979797",
      chevronRight: "#A4A7A6",
      menuIcon: "#b9b9b9"
    },
    red: "#F00"
  },
  // border: "#169580",
  border: "#FFF",
  disabled: "#B7B5B5"
};

export const backgroundColors = {
  balance: "#EEEFEE"
};

export const fontSizes = {
  header: getReponsive.fontSize(16),
  button: getReponsive.fontSize(18),
  // button: 18,
  title: getReponsive.fontSize(16),
  bigtitle: getReponsive.fontSize(28),
  tab: 14,
  balance: 40,
  detail: {
    extremeLarge: getReponsive.fontSize(34),
    largerTitle: getReponsive.fontSize(22),
    largeTitle: getReponsive.fontSize(20),
    huge: getReponsive.fontSize(18),
    bigger: 17,
    big2: getReponsive.fontSize(16),
    big: getReponsive.fontSize(14),
    normal: 15,
    lightBig: 13,
    // small: 12,
    small: getReponsive.fontSize(12),
    tiny: getReponsive.fontSize(9)
  }
};
